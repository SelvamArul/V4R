SET(OPENCV_RECOG_DEPS v4r_core v4r_registration v4r_io v4r_common v4r_features v4r_keypoints v4r_reconstruction v4r_recognition edt ceres)

v4r_check_dependencies(${OPENCV_RECOG_DEPS})

if(NOT V4R_DEPENDENCIES_FOUND)
  message(***RTMT does not meed dependencies*****)
  return()
endif()

v4r_include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
v4r_include_modules(${OPENCV_RECOG_DEPS})

if(WIN32)
  link_directories(${CMAKE_CURRENT_BINARY_DIR})
endif()
if(HAVE_PCL)
  v4r_include_directories(${PCL_INCLUDE_DIRS})
  list(APPEND DEP_LIBS ${PCL_LIBRARIES})
endif()

if(HAVE_SIFTGPU)
  v4r_include_directories(${SIFTGPU_INCLUDE_DIRS})
  list(APPEND DEP_LIBS ${SIFTGPU_LIBRARIES})
endif()

if(HAVE_OPENCV)
  v4r_include_directories(${OPENCV_INCLUDE_DIRS})
  list(APPEND DEP_LIBS ${OPENCV_LIBRARIES})
endif()

if(HAVE_EDT)
  v4r_include_directories(${EDT_INCLUDE_DIRS})
  list(APPEND DEP_LIBS ${EDT_LIBRARIES})
endif()

if(HAVE_GLM)
  v4r_include_directories(${GLM_INCLUDE_DIRS})
else()
  message(***RTMT requires GLM*****)
  return()
endif()

if(HAVE_OPENGL)
  v4r_include_directories(${OPENGL_INCLUDE_DIRS})
  list(APPEND DEP_LIBS ${OPENGL_LIBRARIES})
endif()

if(HAVE_CERES)
  v4r_include_directories(${CERES_INCLUDE_DIRS})
  list(APPEND DEP_LIBS ${CERES_LIBRARIES})
endif()

if(HAVE_BOOST)
  v4r_include_directories(${BOOST_INCLUDE_DIRS})
  list(APPEND DEP_LIBS ${BOOST_LIBRARIES})
endif()

if(HAVE_OPENNI2)
  v4r_include_directories(${OPENNI2_INCLUDE_DIRS})
  list(APPEND DEP_LIBS ${OPENNI2_LIBRARIES})
endif()

set(SOURCES
        CsvReader.cpp
        CsvWriter.cpp
        ModelRegistration.cpp
        Mesh.cpp
        Model.cpp
        PnPProblem.cpp
        Utils.cpp
        RobustMatcher.cpp
)

set(HEADERS
        CsvReader.h
        CsvWriter.h
        ModelRegistration.h
        Mesh.h
        Model.h
        PnPProblem.h
        Utils.h
)


find_package(PkgConfig REQUIRED)
find_package(OpenCV REQUIRED )
find_package(Boost REQUIRED COMPONENTS filesystem system program_options)
pkg_search_module(GL REQUIRED gl)

include_directories(${OpenCV_INCLUDE_DIRS})

add_definitions(-DEIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET)
add_definitions(-Dlinux -D__x86_64__)


include_directories(${CMAKE_CURRENT_BINARY_DIR} ${OPENNI2_INCLUDE_DIR})

add_executable(pnp_detection main_detection.cpp CsvReader ${SOURCES} ${HEADERS})
target_link_libraries(pnp_detection ${OPENCV_RECOG_DEPS} ${DEP_LIBS} ${QT_LIBRARIES} ${OpenCV_LIBS})

add_executable(registration_v4r2opencv main_registration_from_v4r_model.cpp CsvReader ${SOURCES} ${HEADERS})
target_link_libraries(registration_v4r2opencv ${OPENCV_RECOG_DEPS} ${DEP_LIBS} ${QT_LIBRARIES} ${OpenCV_LIBS})

add_executable(recognizer_opencv recognizer_opencv.cpp ${SOURCES} ${HEADERS})
target_link_libraries(recognizer_opencv ${OPENCV_RECOG_DEPS} ${DEP_LIBS} ${QT_LIBRARIES} ${OpenCV_LIBS} ${Boost_LIBRARIES})


INSTALL(TARGETS pnp_detection
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
)

