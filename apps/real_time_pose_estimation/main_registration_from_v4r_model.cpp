// C++
#include <iostream>
// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/features2d/features2d.hpp>
// PnP Tutorial
#include "Mesh.h"
#include "Model.h"
#include "PnPProblem.h"
#include "RobustMatcher.h"
#include "ModelRegistration.h"
#include "Utils.h"


#include <v4r/recognition/multi_pipeline_recognizer.h>

#include <v4r_config.h>
#include <omp.h>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <glog/logging.h>

#include <v4r/recognition/ghv.h>
#include <v4r/recognition/registered_views_source.h>
#include <v4r/features/shot_local_estimator_omp.h>
#include <v4r/features/opencv_sift_local_estimator.h>
#include <v4r/recognition/local_recognizer.h>
#include <v4r/common/miscellaneous.h>



using namespace cv;
using namespace std;
using namespace v4r;

/**  GLOBAL VARIABLES  **/

string tutorial_path = "../../samples/cpp/tutorial_code/calib3d/real_time_pose_estimation/"; // path to tutorial

string img_path = tutorial_path + "Data/resized_IMG_3875.JPG";  // image to register
string ply_read_path = tutorial_path + "Data/box.ply";          // object mesh
string write_path = tutorial_path + "Data/cookies_ORB.yml";     // output file

// Boolean the know if the registration it's done
bool end_registration = false;

// Intrinsic camera parameters: UVC WEBCAM
double f = 45; // focal length in mm
double sx = 22.3, sy = 14.9;
double width = 2592, height = 1944;
double params_CANON[] = { 1380.9,   // fx
                          1380.9,  // fy
                          937.4334,      // cx
                          545.1564};    // cy

// Setup the points to register in the image
// In the order of the *.ply file and starting at 1
int n = 8;
int pts[] = {1, 2, 3, 4, 5, 6, 7, 8}; // 3 -> 4

// Some basic colors
Scalar red(0, 0, 255);
Scalar green(0,255,0);
Scalar blue(255,0,0);
Scalar yellow(0,255,255);

/*
 * CREATE MODEL REGISTRATION OBJECT
 * CREATE OBJECT MESH
 * CREATE OBJECT MODEL
 * CREATE PNP OBJECT
 */
ModelRegistration registration;

Mesh mesh;
PnPProblem pnp_registration(params_CANON);

/**  Functions headers  **/
void help();

// Mouse events for model registration
/**  Main program  **/

typedef pcl::PointXYZRGB PointT;
typedef v4r::Model<PointT> ModelT;
typedef boost::shared_ptr<ModelT> ModelTPtr;

int main()
{
    typename v4r::LocalRecognitionPipeline<pcl::PointXYZRGB>::Parameter paramLocalRecSift;
    paramLocalRecSift.use_cache_ = true;
    paramLocalRecSift.save_hypotheses_  = true;

    std::string models_dir = "/home/aura/software/v4r_final/build/data/models";

    boost::shared_ptr < RegisteredViewsSource<pcl::PointXYZRGBNormal, pcl::PointXYZRGB, pcl::PointXYZRGB> > src
            (new RegisteredViewsSource<pcl::PointXYZRGBNormal, pcl::PointXYZRGB, pcl::PointXYZRGB>(0.01));
    boost::shared_ptr <Source<pcl::PointXYZRGB> > cast_source;

    src->setPath (models_dir);
    src->generate ();
    cast_source = boost::static_pointer_cast<RegisteredViewsSource<pcl::PointXYZRGBNormal, pcl::PointXYZRGB, pcl::PointXYZRGB> > (src);

    boost::shared_ptr<LocalRecognitionPipeline<pcl::PointXYZRGB> > sift_r;
    std::vector<ModelTPtr> models = cast_source->getModels();


    for (size_t m_id = 0; m_id < models.size (); m_id++)
    {
        ModelTPtr m = models[m_id];
        opencv_pose::Model global_model;
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr global_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);


        const std::string out_train_path = models_dir  + "/" + m->class_ + "/" + m->id_ + "/opencv_models/";
        const std::string in_train_path = models_dir  + "/" + m->class_ + "/" + m->id_ + "/views/";
        std::cout << "Processing class "  << m->class_ << std::endl;

        boost::filesystem::path dir(out_train_path);
        if(boost::filesystem::create_directory(dir)){
            std::cerr<< "Directory Created: "<<out_train_path<<std::endl;
        }

        if(!cast_source->getLoadIntoMemory())
            cast_source->loadInMemorySpecificModel(*m);

        for(size_t v_id=0; v_id< m->view_filenames_.size(); v_id++){
            opencv_pose::Model model;
            std::string write_path = out_train_path + "/" + m->view_filenames_[v_id];
            std::string pose_fn = in_train_path + "/" + m->view_filenames_[v_id];
            boost::replace_last(write_path, ".pcd", ".yml");
            boost::replace_last(write_path, "cloud", "model");
            boost::replace_last(pose_fn, ".pcd", ".txt");
            boost::replace_last(pose_fn, "cloud", "pose");
            std::cout << "pose_fn " << pose_fn << std::endl;
            Eigen::Matrix4f pose = io::readMatrixFromFile( pose_fn );

            std::cout << "Processing view " << m->view_filenames_[v_id]<< std::endl;
            std::cout << "Writing in " << write_path<< std::endl;
            std::vector<std::vector<float> > all_signatures;
            std::vector<std::vector<float> > object_signatures;
            typename pcl::PointCloud<PointT> all_keypoints;
            typename pcl::PointCloud<PointT> object_keypoints;
            pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr transformed_cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
            pcl::PointCloud<PointT>::Ptr cloud = m->views_[v_id];
            std::cout << "cloud " << cloud->width << "x" << cloud->height<< std::endl;
            std::vector<int> all_kp_indices, obj_kp_indices;

            std::cout << "Hallo " << (*m->views_[v_id]).width << (*m->views_[v_id]).height << std::endl;




            cv::Mat colorImage = ConvertPCLCloud2Image(*m->views_[v_id]);
            //cv::imshow("inputimage", colorImage);
            //cv::waitKey(10);
            //std::vector<SiftGPU::SiftKeypoint> ks;
            std::vector<KeyPoint> ks;

            cv::Mat grayImage;
            cv::cvtColor (colorImage, grayImage, CV_BGR2GRAY);

            cv::Mat descriptors;
            cv::SiftFeatureDetector detector(40000);
            cv::SiftDescriptorExtractor extractor;

            detector.detect( grayImage, ks );

            extractor.compute( grayImage, ks, descriptors );

            //use indices_ to check if the keypoints and feature should be saved
            //compute SIFT keypoints and SIFT features
            //backproject sift keypoints to 3D and save in keypoints
            //save signatures
            std::vector<float> scales;
            scales.resize(ks.size());
            all_signatures.resize (ks.size (), std::vector<float>(128));

            for(size_t i=0; i < ks.size(); i++){
                for (int k = 0; k < 128; k++)
                    all_signatures[i][k] = descriptors.at<float>(i,k);

                scales[i] = ks[i].size;
            }


            for(unsigned int i = 0 ; i < ks.size(); i++){
                  cv::circle(grayImage,cv::Point2f(ks[i].pt.x, ks[i].pt.y),ks[i].size,CV_RGB(255,255,0));
            }

            std::cout << "Number of SIFT features size:" << ks.size() << std::endl;

            all_keypoints.resize(ks.size());
            all_kp_indices.resize(ks.size());


            std::vector<bool> obj_mask;
            obj_mask.resize((*m->views_[v_id]).width * (*m->views_[v_id]).height, true);

            size_t kept = 0;
            for(size_t i=0; i < ks.size(); i++){
                const unsigned int v = (int)(ks[i].pt.y+.5);
                const unsigned int u = (int)(ks[i].pt.x+.5);
                const int idx = v * (*m->views_[v_id]).width + u;

                if(u >= 0 && v >= 0 && u < (*m->views_[v_id]).width && v < (*m->views_[v_id]).height && pcl::isFinite((*m->views_[v_id]).points[idx]) && obj_mask[idx]){
                    all_keypoints.points[kept] = (*m->views_[v_id]).points[idx];
                    all_kp_indices[kept] = idx;
                    scales[kept] = scales[i];
                    all_signatures[kept] = all_signatures[i];
                    kept++;
                }
            }

            all_keypoints.points.resize(kept);
            scales.resize(kept);
            all_kp_indices.resize(kept);
            all_signatures.resize(kept);


            std::cout << "Number of SIFT features kept:" << kept << std::endl;
            obj_mask.clear();

            // remove signatures and keypoints which do not belong to object
            obj_mask = createMaskFromIndices(m->indices_[v_id].indices, m->views_[v_id]->points.size());
            obj_kp_indices.resize( all_kp_indices.size() );
            object_signatures.resize( all_kp_indices.size() ) ;
            kept=0;
            for (size_t kp_id = 0; kp_id < all_kp_indices.size(); kp_id++)
            {
                int idx = all_kp_indices[kp_id];
                if ( obj_mask[idx] )
                {
                    obj_kp_indices[kept] = idx;
                    object_signatures[kept] = all_signatures[kp_id];
                    kept++;
                    int v = idx / (*m->views_[v_id]).width;
                    int u = idx % (*m->views_[v_id]).width;
                    cv::circle(colorImage,cv::Point2f(u, v),2,CV_RGB(255,255,0));

                }
            }
            object_signatures.resize( kept );
            obj_kp_indices.resize( kept );
            cv::imshow("inputimage", colorImage);
            cv::waitKey(10);

            pcl::transformPointCloud (*cloud, *transformed_cloud, pose);
            std::cout << "HEEEERRRe"<< std::endl;

            for (size_t kp_id = 0; kp_id < obj_kp_indices.size(); kp_id++){
                int idx = obj_kp_indices[kp_id];
                int v = idx / (*m->views_[v_id]).width;
                int u = idx % (*m->views_[v_id]).width;
                Point2f point2d(u,v);
                //Point3f point3d(transformed_cloud->points[kp_id].x ,transformed_cloud->points[kp_id].y,transformed_cloud->points[kp_id].z);
                Point3f point3d(transformed_cloud->points[idx].x ,transformed_cloud->points[idx].y,transformed_cloud->points[idx].z);
                std::cout <<  "Point3f " << point3d << std::endl;
                cv::KeyPoint key(point2d,1);
                model.add_correspondence(point2d, point3d);
                cv::Mat desc(object_signatures[kp_id], true);
                model.add_descriptor(desc);
                model.add_keypoint(key);

                global_model.add_correspondence(point2d, point3d);
                global_model.add_descriptor(desc);
                global_model.add_keypoint(key);

                global_cloud->push_back(transformed_cloud->points[idx]);

            }

            // save the model into a *.yaml file
            model.save(write_path);
            ;


        }

        global_model.save(out_train_path + "/global_model.yml");
        pcl::io::savePCDFileASCII (out_train_path + "global_model.pcd", *global_cloud);
        if(!cast_source->getLoadIntoMemory())
            m->views_.clear();
    }
    std::cout << "Finished processing all models" << std::endl;
    return 0;
  cout << "GOODBYE" << endl;

  // save the model into a *.yaml file


}

/**********************************************************************************************************/
void help()
{
  cout
  << "--------------------------------------------------------------------------"   << endl
  << "This program shows how to create your 3D textured model. "                    << endl
  << "Usage:"                                                                       << endl
  << "./cpp-tutorial-pnp_registration"                                              << endl
  << "--------------------------------------------------------------------------"   << endl
  << endl;
}
