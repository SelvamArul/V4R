#include <v4r/common/miscellaneous.h>  // to extract Pose intrinsically stored in pcd file

#include <v4r/io/filesystem.h>
#include <v4r/recognition/multi_pipeline_recognizer.h>

#include <pcl/common/time.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/common/transforms.h>
#include <pcl/common/centroid.h>


#include <iostream>
#include <sstream>

#include <boost/any.hpp>
#include <boost/program_options.hpp>
#include <glog/logging.h>

#include <opencv2/opencv.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/features2d/features2d.hpp>
#include "opencv2/nonfree/features2d.hpp"
// PnP Tutorial
#include "Mesh.h"
#include "Model.h"
#include "PnPProblem.h"
#include "RobustMatcher.h"
#include "ModelRegistration.h"
#include "Utils.h"

#include <pcl/io/pcd_io.h>
#include <v4r/recognition/ghv.h>
#include <v4r/recognition/registered_views_source.h>
#include <v4r/features/shot_local_estimator_omp.h>
#include <v4r/common/miscellaneous.h>

#include <v4r/common/pcl_opencv.h>


namespace po = boost::program_options;
namespace fs = boost::filesystem;
using namespace v4r;
using namespace std;
using namespace cv;
typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud;

void initKalmanFilter( KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt);
void updateKalmanFilter( KalmanFilter &KF, Mat &measurements,
                         Mat &translation_estimated, Mat &rotation_estimated );
void fillMeasurements( Mat &measurements,
                       const Mat &translation_measured, const Mat &rotation_measured);

int main(int argc, char** argv){

    double f = 1380.9;                           // focal length in mm
    double sx = 937.4334, sy = 14.9;             // sensor size
    double width = 1920, height = 1080;        // image size

    double params_WEBCAM[] = { 1380.9,   // fx
                               1380.9,  // fy
                               937.4334,      // cx
                               545.1564};    // cy

    // Some basic colors
    Scalar red(0, 0, 255);
    Scalar green(0,255,0);
    Scalar blue(255,0,0);
    Scalar yellow(0,255,255);

    KalmanFilter KF;         // instantiate Kalman Filter
    int nStates = 18;            // the number of states
    int nMeasurements = 6;       // the number of measured states
    int nInputs = 0;             // the number of control actions
    double dt = 0.125;           // time between measurements (1/FPS)


    std::string pathStr = ".";
    std::string modelPath = ".";
    std::string className = ".";
    int numKeyPoints = 2000;      // number of detected keypoints
    float ratioTest = 0.70f;          // ratio test
    bool fast_match = false;       // fastRobustMatch() or robustMatch()
    RobustMatcher rmatcher;                                                     // instantiate RobustMatcher
    PnPProblem pnp_detection(params_WEBCAM);
    PnPProblem pnp_detection_est(params_WEBCAM);

    // RANSAC parameters
    int iterationsCount = 500;      // number of Ransac iterations.
    float reprojectionError = 2.0;  // maximum allowed distance to consider it an inlier.
    double confidence = 0.95;        // ransac successful confidence.

    // Kalman Filter parameters
    int minInliersKalman = 30;    // Kalman threshold updating

    // PnP parameters
    int pnpMethod = cv::ITERATIVE;

    unsigned int finished = 0;
    bool good_measurement = false;
    Mat measurements(nMeasurements, 1, CV_64F); measurements.setTo(Scalar(0));

    typedef pcl::PointXYZRGB PointT;
    typedef v4r::Model<PointT> ModelT;
    typedef boost::shared_ptr<ModelT> ModelTPtr;

    typedef pcl::PointXYZRGB PointT;
    std::string test_dir;
    std::string output_folder = "";
    bool visualize = true;
    double chop_z = std::numeric_limits<double>::max();
    std::string mask = "";
    opencv_pose::Model model;               // instantiate Model object
    std::string model_path;
    time_t start, end;

    // fps calculated using number of frames / seconds
    // floating point seconds elapsed since start
    double fps, sec;
    bool use_opencv_recog = true;
    bool use_v4r_recog = false;
    bool do_icp = false;

    // frame counter
    int counter = 0;

    // start the clock
    time(&start);
    std::string dense_model = "";


    po::options_description desc("Single-View Object Instance Recognizer\n======================================\n**Allowed options");
    desc.add_options()
        ("help,h", "produce help message")
        ("visualize,v", po::bool_switch(&visualize), "visualize recognition results")
        ("do_icp", po::bool_switch(&do_icp), "Apply icp after initialization")
        ("use_v4r_recog", po::bool_switch(&use_v4r_recog), "visualize recognition results")
        ("mask,m", po::value<std::string>(&mask), "Mask input cloud with given class")
        ("model_path", po::value<std::string>(&model_path)->required(), "Model path with yml extension")
        ("chop_z,z", po::value<double>(&chop_z)->default_value(chop_z, boost::str(boost::format("%.2e") % chop_z) ), "points with z-component higher than chop_z_ will be ignored (low chop_z reduces computation time and false positives (noise increase with z)")
        ("path,p",po::value(&pathStr)->required(),"Path to operate on")
        ("dense_model",po::value<std::string>(&dense_model),"Path to operate on")
        ("output_folder,o",po::value<std::string>(&output_folder),"If present, will save the the images there")
   ;

    std::cout << "Flag 1" << std::endl;
    po::variables_map vm;
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(desc).allow_unregistered().run();
    po::store(parsed, vm);
    if (vm.count("help")) { std::cout << desc << std::endl; }
    try { po::notify(vm); }
    catch(std::exception& e) { std::cerr << "Error: " << e.what() << std::endl << std::endl << desc << std::endl;  }




    PointCloud::Ptr dense_cloud(new PointCloud);
    pcl::io::loadPCDFile(dense_model, *dense_cloud);

    model.load(model_path); // load a 3D textured object model
    // Get the MODEL INFO
    vector<Point3f> list_points3d_model = model.get_points3d();  // list with model 3D coordinates
    Mat descriptors_model = model.get_descriptors();                  // list with descriptors of each 3D coordinate


    initKalmanFilter(KF, nStates, nMeasurements, nInputs, dt);    // init function

    // Gather list of pcd
    std::vector<fs::path> filesPath;
    {
        auto it = fs::recursive_directory_iterator(pathStr);
        auto end = fs::recursive_directory_iterator();

        for(; it != end; ++it)
        {
            fs::path path = *it;
            std::cout << "Checking folder : " << path.string() << std::endl;
            if(fs::is_directory(path) && fs::exists(path / "frame.pcd"))
                filesPath.push_back(fs::canonical(path));
        }
    }
    v4r::MultiRecognitionPipeline<PointT> r(argc, argv);
    std::string maskFilename = "mask_"+mask+".png";
    std::cout << "Number of files to process: " << filesPath.size() << std::endl;

    for (int i = 0; i < filesPath.size(); i++){
        std::cout << "Recognizing file " << filesPath[i].string() << std::endl;
        LOG(INFO) << "Recognizing file " << filesPath[i];
        PointCloud::Ptr input(new PointCloud);
        PointCloud::Ptr output(new PointCloud);

        pcl::io::loadPCDFile((filesPath[i] / "frame.pcd").string(), *input);
        if(!mask.empty()){
            if(!fs::is_directory(filesPath[i]) || !fs::exists(filesPath[i] / maskFilename)){
                std::cout << "Class " << mask << " is not present " << std::endl;
                continue;
            }

            cv::Mat_<uint8_t> mask_box = cv::imread((filesPath[i] / maskFilename).string(), CV_LOAD_IMAGE_GRAYSCALE);
            //cv::imshow("Mask", mask_box);
            //cv::waitKey(0);
            std::cout << "Output has " << input->points.size () << " points." << std::endl;
            pcl::PointIndices indices;
            int nCols = mask_box.cols;
            int nRows = mask_box.rows;
            uint8_t* p;
            std::cout << "nRows " << nRows << " nCols" << nCols << std::endl;
            int counter = 0;
            for(int i = 0; i < nRows; ++i){
                p = mask_box.ptr<uint8_t>(i);
                for (int j = 0; j < nCols; ++j){
                    //std::cout << ((int) p[j]) << std::endl;
                    if(((int) p[j])==255){
                        indices.indices.push_back(i*nCols + j);
                        counter++;
                        //std::cout << "correct point" << std::endl;
                    }
                }
            }


            pcl::ExtractIndices<PointT> extract_indices;
            extract_indices.setIndices (boost::make_shared<const pcl::PointIndices> (indices));
            extract_indices.setInputCloud (input);
            extract_indices.setKeepOrganized (true);
            extract_indices.filterDirectly(input);
            pcl::io::savePCDFileASCII ("/home/aura/apc/test_pcd.pcd", *input);
            std::cout << "Output has " << input->points.size () << " points." << std::endl;
            std::cout << "indices size " << indices.indices.size()<< " vs counter" << counter << std::endl;
        }


        if( chop_z > 0){
            pcl::PassThrough<PointT> pass;
            pass.setFilterLimits ( 0.f, chop_z );
            pass.setFilterFieldName ("z");
            pass.setInputCloud (input);
            pass.setKeepOrganized (true);
            pass.filter (*input);
        }

        int num_verified_models = 0;
        if(use_v4r_recog){
            //First try with v4r Library
            r.setInputCloud (input);
            std::cout << "inputting cloud " << std::endl;
            r.recognize();
            std::cout << "recognize" << std::endl;

            std::vector<ModelTPtr> verified_models = r.getVerifiedModels();
            std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > transforms_verified;
            transforms_verified = r.getVerifiedTransforms();

            if (visualize){
                std::cout << "about to visualize" << std::endl;
                r.visualize();
            }

            num_verified_models = verified_models.size();

            for(size_t m_id=0; m_id<verified_models.size(); m_id++)
                LOG(INFO) << "********************" << verified_models[m_id]->id_ << std::endl;
        }

        // Instantiate estimated translation and rotation
        Mat translation_estimated= cv::Mat::zeros(3, 1, CV_64F);
        Mat rotation_estimated = cv::Mat::eye(3, 3, CV_64F);
        bool valid_estimation =false;

        if(num_verified_models==0 || use_opencv_recog){
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
            pcl::copyPointCloud(*input, *cloud);

            cv::Mat frame = v4r::ConvertPCLCloud2Image(*cloud);
            cv::Mat frame_vis = frame.clone();    // refresh visua7lisation frame
            cv::imshow("Frame", frame_vis);
            cv::waitKey(1);



          // -- Step 1: Robust matching between model descriptors and scene descriptors

          vector<DMatch> good_matches;       // to obtain the 3D points of the model
          vector<KeyPoint> keypoints_scene;  // to obtain the 2D points of the scene


          std::cout << "Blah 1" << std::endl;
          if(fast_match){
              std::cout << "Blah 1" << std::endl;
            rmatcher.fastRobustMatch(frame, good_matches, keypoints_scene, descriptors_model);
            std::cout << "Blah 1" << std::endl;
          } else {
            rmatcher.robustMatch(frame, good_matches, keypoints_scene, descriptors_model);
          }

          std::cout << "Blah 2" << std::endl;

          // -- Step 2: Find out the 2D/3D correspondences

          vector<Point3f> list_points3d_model_match; // container for the model 3D coordinates found in the scene
          vector<Point2f> list_points2d_scene_match; // container for the model 2D coordinates found in the scene

          std::cout << "Blah 3" << std::endl;
          for(unsigned int match_index = 0; match_index < good_matches.size(); ++match_index)
          {
            Point3f point3d_model = list_points3d_model[ good_matches[match_index].trainIdx ];  // 3D point from model
            Point2f point2d_scene = keypoints_scene[ good_matches[match_index].queryIdx ].pt; // 2D point from the scene
            list_points3d_model_match.push_back(point3d_model);         // add 3D point
            list_points2d_scene_match.push_back(point2d_scene);         // add 2D point
          }

          // Draw outliers
          draw2DPoints(frame_vis, list_points2d_scene_match, red);


          Mat inliers_idx;
          vector<Point2f> list_points2d_inliers;

          if(good_matches.size() > 0) // None matches, then RANSAC crashes
          {

            // -- Step 3: Estimate the pose using RANSAC approach
            pnp_detection.estimatePoseRANSAC( list_points3d_model_match, list_points2d_scene_match,
                                              pnpMethod, inliers_idx,
                                              iterationsCount, reprojectionError, confidence );

            // -- Step 4: Catch the inliers keypoints to draw
            for(int inliers_index = 0; inliers_index < inliers_idx.rows; ++inliers_index)
            {
              int n = inliers_idx.at<int>(inliers_index);         // i-inlier
              Point2f point2d = list_points2d_scene_match[n]; // i-inlier point 2D
              list_points2d_inliers.push_back(point2d);           // add i-inlier to list
            }

            // Draw inliers points 2D
            draw2DPoints(frame_vis, list_points2d_inliers, blue);


            // -- Step 5: Kalman Filter

            good_measurement = false;

            // GOOD MEASUREMENT
            if( inliers_idx.rows >= minInliersKalman )
            {

              // Get the measured translation
              Mat translation_measured(3, 1, CV_64F);
              translation_measured = pnp_detection.get_t_matrix();

              // Get the measured rotation
              Mat rotation_measured(3, 3, CV_64F);
              rotation_measured = pnp_detection.get_R_matrix();

              // fill the measurements vector
              fillMeasurements(measurements, translation_measured, rotation_measured);

              good_measurement = true;

            }



            // update the Kalman filter with good measurements
            updateKalmanFilter( KF, measurements,
                                translation_estimated, rotation_estimated);


            // -- Step 6: Set estimated projection matrix
            pnp_detection_est.set_P_matrix(rotation_estimated, translation_estimated);

          }

          // -- Step X: Draw pose

          if(good_measurement)
          {
              std::cout << "Confident" << std::endl;
            //drawObjectMesh(frame_vis, &mesh, &pnp_detection, green);  // draw current pose
          } else {
            std::cout << "Not Confident" << std::endl;
            //drawObjectMesh(frame_vis, &mesh, &pnp_detection_est, yellow); // draw estimated pose
          }

          float l = 5;
          vector<Point2f> pose_points2d;
          pose_points2d.push_back(pnp_detection_est.backproject3DPoint(Point3f(0,0,0)));  // axis center
          pose_points2d.push_back(pnp_detection_est.backproject3DPoint(Point3f(l,0,0)));  // axis x
          pose_points2d.push_back(pnp_detection_est.backproject3DPoint(Point3f(0,l,0)));  // axis y
          pose_points2d.push_back(pnp_detection_est.backproject3DPoint(Point3f(0,0,l)));  // axis z
          draw3DCoordinateAxes(frame_vis, pose_points2d);           // draw axes

          // FRAME RATE

          // see how much time has elapsed
          time(&end);

          // calculate current FPS
          ++counter;
          sec = difftime (end, start);

          fps = counter / sec;

          drawFPS(frame_vis, fps, yellow); // frame ratio
          double detection_ratio = ((double)inliers_idx.rows/(double)good_matches.size())*100;
          drawConfidence(frame_vis, detection_ratio, yellow);


          // -- Step X: Draw some debugging text

          // Draw some debug text
          int inliers_int = inliers_idx.rows;
          int outliers_int = (int)good_matches.size() - inliers_int;
          string inliers_str = IntToString(inliers_int);
          string outliers_str = IntToString(outliers_int);
          string n = IntToString((int)good_matches.size());
          string text = "Found " + inliers_str + " of " + n + " matches";
          string text2 = "Inliers: " + inliers_str + " - Outliers: " + outliers_str;

          drawText(frame_vis, text, green);
          drawText2(frame_vis, text2, red);

          imshow("REAL TIME DEMO", frame_vis);
          cv::waitKey(0);

          if(do_icp){

              Eigen::Matrix4f transform_1 = Eigen::Matrix4f::Identity();
              if(good_measurement){

                  // Define a rotation matrix (see https://en.wikipedia.org/wiki/Rotation_matrix)
                  transform_1 (0,0) = rotation_estimated.at<double>(0,0);
                  transform_1 (0,1) = rotation_estimated.at<double>(0,1);
                  transform_1 (0,2) = rotation_estimated.at<double>(0,2);
                  transform_1 (1,0) = rotation_estimated.at<double>(1,0);
                  transform_1 (1,1) = rotation_estimated.at<double>(1,1);
                  transform_1 (1,2) = rotation_estimated.at<double>(1,2);
                  transform_1 (2,0) = rotation_estimated.at<double>(2,0);
                  transform_1 (2,1) = rotation_estimated.at<double>(2,1);
                  transform_1 (2,2) = rotation_estimated.at<double>(2,2);
                  transform_1 (0,3) = translation_estimated.at<double>(0,3);
                  transform_1 (1,3) = translation_estimated.at<double>(1,3);
                  transform_1 (2,3) = translation_estimated.at<double>(2,3);
              } else {
                  Eigen::Vector4f centroid_model;
                  Eigen::Vector4f centroid_scene;
                  pcl::compute3DCentroid(*input, centroid_scene);
                  pcl::compute3DCentroid(*dense_cloud, centroid_model);
                  transform_1 (0,3) = centroid_model[0] - centroid_scene[0];
                  transform_1 (1,3) = centroid_model[1] - centroid_scene[1];
                  transform_1 (2,3) = centroid_model[2] - centroid_scene[2];
              }

              pcl::PointCloud<pcl::PointXYZRGB>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZRGB> ());
                // You can either apply transform_1 or transform_2; they are the same
              pcl::transformPointCloud (*dense_cloud, *transformed_cloud, transform_1);

              pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
              icp.setInputCloud(transformed_cloud);
              icp.setInputTarget(input);
              pcl::PointCloud<pcl::PointXYZRGB> Final;
              icp.align(Final);
              std::cout << "has converged:" << icp.hasConverged() << " score: " <<
              icp.getFitnessScore() << std::endl;
              std::cout << icp.getFinalTransformation() << std::endl;
              pcl::io::savePCDFileASCII ("aligned.pcd", Final);
              pcl::io::savePCDFileASCII ("initial.pcd", *transformed_cloud);
              pcl::io::savePCDFileASCII ("target.pcd", *                input);


          }
        }
    }

    return 0;
}



void initKalmanFilter(KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt)
{

  KF.init(nStates, nMeasurements, nInputs, CV_64F);                 // init Kalman Filter

  setIdentity(KF.processNoiseCov, Scalar::all(1e-5));       // set process noise
  setIdentity(KF.measurementNoiseCov, Scalar::all(1e-2));   // set measurement noise
  setIdentity(KF.errorCovPost, Scalar::all(1));             // error covariance


                     /** DYNAMIC MODEL **/

  //  [1 0 0 dt  0  0 dt2   0   0 0 0 0  0  0  0   0   0   0]
  //  [0 1 0  0 dt  0   0 dt2   0 0 0 0  0  0  0   0   0   0]
  //  [0 0 1  0  0 dt   0   0 dt2 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  1  0  0  dt   0   0 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  1  0   0  dt   0 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  0  1   0   0  dt 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  0  0   1   0   0 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  0  0   0   1   0 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  0  0   0   0   1 0 0 0  0  0  0   0   0   0]
  //  [0 0 0  0  0  0   0   0   0 1 0 0 dt  0  0 dt2   0   0]
  //  [0 0 0  0  0  0   0   0   0 0 1 0  0 dt  0   0 dt2   0]
  //  [0 0 0  0  0  0   0   0   0 0 0 1  0  0 dt   0   0 dt2]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  1  0  0  dt   0   0]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  0  1  0   0  dt   0]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  1   0   0  dt]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   1   0   0]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   1   0]
  //  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   0   1]

  // position
  KF.transitionMatrix.at<double>(0,3) = dt;
  KF.transitionMatrix.at<double>(1,4) = dt;
  KF.transitionMatrix.at<double>(2,5) = dt;
  KF.transitionMatrix.at<double>(3,6) = dt;
  KF.transitionMatrix.at<double>(4,7) = dt;
  KF.transitionMatrix.at<double>(5,8) = dt;
  KF.transitionMatrix.at<double>(0,6) = 0.5*pow(dt,2);
  KF.transitionMatrix.at<double>(1,7) = 0.5*pow(dt,2);
  KF.transitionMatrix.at<double>(2,8) = 0.5*pow(dt,2);

  // orientation
  KF.transitionMatrix.at<double>(9,12) = dt;
  KF.transitionMatrix.at<double>(10,13) = dt;
  KF.transitionMatrix.at<double>(11,14) = dt;
  KF.transitionMatrix.at<double>(12,15) = dt;
  KF.transitionMatrix.at<double>(13,16) = dt;
  KF.transitionMatrix.at<double>(14,17) = dt;
  KF.transitionMatrix.at<double>(9,15) = 0.5*pow(dt,2);
  KF.transitionMatrix.at<double>(10,16) = 0.5*pow(dt,2);
  KF.transitionMatrix.at<double>(11,17) = 0.5*pow(dt,2);


           /** MEASUREMENT MODEL **/

  //  [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
  //  [0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
  //  [0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
  //  [0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0]
  //  [0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0]
  //  [0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0]

  KF.measurementMatrix.at<double>(0,0) = 1;  // x
  KF.measurementMatrix.at<double>(1,1) = 1;  // y
  KF.measurementMatrix.at<double>(2,2) = 1;  // z
  KF.measurementMatrix.at<double>(3,9) = 1;  // roll
  KF.measurementMatrix.at<double>(4,10) = 1; // pitch
  KF.measurementMatrix.at<double>(5,11) = 1; // yaw

}

/**********************************************************************************************************/
void updateKalmanFilter( KalmanFilter &KF, Mat &measurement,
                         Mat &translation_estimated, Mat &rotation_estimated )
{

  // First predict, to update the internal statePre variable
  Mat prediction = KF.predict();

  // The "correct" phase that is going to use the predicted value and our measurement
  Mat estimated = KF.correct(measurement);

  // Estimated translation
  translation_estimated.at<double>(0) = estimated.at<double>(0);
  translation_estimated.at<double>(1) = estimated.at<double>(1);
  translation_estimated.at<double>(2) = estimated.at<double>(2);

  // Estimated euler angles
  Mat eulers_estimated(3, 1, CV_64F);
  eulers_estimated.at<double>(0) = estimated.at<double>(9);
  eulers_estimated.at<double>(1) = estimated.at<double>(10);
  eulers_estimated.at<double>(2) = estimated.at<double>(11);

  // Convert estimated quaternion to rotation matrix
  rotation_estimated = euler2rot(eulers_estimated);

}

/**********************************************************************************************************/
void fillMeasurements( Mat &measurements,
                       const Mat &translation_measured, const Mat &rotation_measured)
{
  // Convert rotation matrix to euler angles
  Mat measured_eulers(3, 1, CV_64F);
  measured_eulers = rot2euler(rotation_measured);

  // Set measurement to predict
  measurements.at<double>(0) = translation_measured.at<double>(0); // x
  measurements.at<double>(1) = translation_measured.at<double>(1); // y
  measurements.at<double>(2) = translation_measured.at<double>(2); // z
  measurements.at<double>(3) = measured_eulers.at<double>(0);      // roll
  measurements.at<double>(4) = measured_eulers.at<double>(1);      // pitch
  measurements.at<double>(5) = measured_eulers.at<double>(2);      // yaw
}
