#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/common/time.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/octree/octree_pointcloud_voxelcentroid.h>
#include <pcl/octree/octree.h>
#include <pcl/octree/octree_impl.h>
#include <pcl/octree/octree_pointcloud.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/transforms.h>

#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>
#include <string>


#include "params.h"
#include <v4r/keypoints/impl/Object.hpp>
#include <v4r/reconstruction/KeypointSlamRGBD2.h>
#include <v4r/common/impl/DataMatrix2D.hpp>
#include <v4r/keypoints/impl/triple.hpp>
#include <v4r/common/convertCloud.h>
#include <v4r/keypoints/ClusterNormalsToPlanes.h>
#include "OctreeVoxelCentroidContainerXYZRGB.hpp"
#include "sensor.h"
#include "ObjectSegmentation.h"
#include "StoreTrackingModel.h"
#include "BundleAdjustment.h"
#include "MultiSession.h"


#include <iostream>
#include <fstream>

using namespace v4r;
namespace po = boost::program_options;

int main(int argc, char** argv){

    int a = 10;
    std::ofstream myfile;
    myfile.open ("example.txt");
    myfile << "Writing this to a file." << a << "\n";
    myfile.close();

    Sensor* m_sensor(new Sensor());
    std::cout << "Starting process" << std::endl;
    std::vector<std::string> folders;
    std::vector< cv::Mat_<unsigned char> > masks;
    //folders.push_back("/home/aura/Documents/Robotics/working_set");
    //folders.push_back("/media/aura/Data/Datasets/apc/objects/ticonderoga_12_pencils_bottom/");
    //folders.push_back("/media/aura/Data/Datasets/apc/objects/ticonderoga_12_pencils_top/");
    std::string model_name = "ticonderoga_12_pencils";
    std::string class_name = "ticonderoga_12_pencils";
    std::vector<std::string> mask_root;
    std::string add_filename = "";
    std::ofstream add_file;
    // bunch of variable initialization
    //mask_root.push_back("/media/aura/Data/Datasets/apc/objects/ticonderoga_12_pencils_bottom_mask/");
    //mask_root.push_back("/media/aura/Data/Datasets/apc/objects/ticonderoga_12_pencils_top_mask/");
    std::string model_folder = "data";
    bool use_custom_mask = true;
    int roi_x, roi_y;


    po::options_description desc("Run object reconstruction pipelin\n======================================\n**Allowed options");
    desc.add_options()
        ("help,h", "produce help message")
        ("use_mask", po::bool_switch(&use_custom_mask), "visualize recognition results")
        ("class_name",po::value<std::string>(&class_name),"Path to operate on")
        ("model_name",po::value<std::string>(&model_name)->required(),"Path to operate on")
        ("roi_x",po::value<int>(&roi_x)->default_value(1292),"x coordinate of the plane for the ROI") 
        ("roi_y",po::value<int>(&roi_y)->default_value(395),"y coordinate of the plane for the ROI")
        ("folders", po::value<std::vector<std::string> >(&folders)->multitoken(), "Folders to be processed. If several, use multiview alignment")
        ("mask_root", po::value<std::vector<std::string> >(&mask_root)->multitoken(), "Mask folder")
        ("save_added",po::value<std::string>(&add_filename),"Path to operate on")
   ;

    po::variables_map vm;
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(desc).allow_unregistered().run();
    po::store(parsed, vm);
    if (vm.count("help")) { std::cout << desc << std::endl; }
    try { po::notify(vm); }
    catch(std::exception& e) { std::cerr << "Error: " << e.what() << std::endl << std::endl << desc << std::endl;  }


    if(!add_filename.empty()){
        std::cout << "Creating file " << add_filename << std::endl;
        add_file.open (add_filename);
        add_file << "Writing this to a file.\n";
    }




    std::cout << "folder" << folders.size() << std::endl;
    v4r::DataMatrix2D<Eigen::Vector3f> kp_cloud;
    Eigen::Matrix4f pose(Eigen::Matrix4f::Identity());
    cv::Mat_<cv::Vec3b> image;
    bool m_run_tracker = true;
    BundleAdjustment* m_ba(new BundleAdjustment());
    m_ba->m_emit_signals = false;
    ObjectSegmentation* m_segmentation(new ObjectSegmentation());
    bool m_first_iter = true;
    MultiSession* m_multi_session(new MultiSession());
    bool is_conf;
    int type=0;
    bool have_multi_session = (folders.size()>1)? true: false;
    m_sensor->m_draw_mask = true;

    BundleAdjustmentParameter ba_params;
    ba_params.dist_cam_add_projections = 0.02;

    ObjectModelling om_params;
    om_params.vx_size_object = 0.002;
    m_segmentation->object_modelling_parameter_changed(om_params);
	
    // intrinsic params
    RGBDCameraParameter cam_params_fixed;
    cam_params_fixed.f[0] = 1380.9;
    cam_params_fixed.f[1] = 1380.9;
    cam_params_fixed.c[0] = 937.4334;
    cam_params_fixed.c[1] = 545.1564;


    CamaraTrackerParameter cam_tracker_params;
    cam_tracker_params.log_point_clouds = true;
    cam_tracker_params.create_prev_cloud = true;
    cam_tracker_params.min_delta_angle = 15;
    cam_tracker_params.min_delta_distance = 1;
    cam_tracker_params.prev_voxegrid_size = 0.01;
    cam_tracker_params.prev_z_cutoff = 2;




    SegmentationParameter seg_params;
    seg_params.inl_dist_plane = 0.01;
    seg_params.thr_angle = 30;
    seg_params.min_points_plane = 7000;
    m_segmentation->segmentation_parameter_changed(seg_params);

    m_segmentation->set_segmentation_params(true, 0.01, false,3.0);

    std::cout << "Finished intialization" << std::endl;
    for(int j = 0 ; j < folders.size(); j++){
        masks.clear();

        std::string m_dataset_folder = folders[j];
        std::vector<boost::filesystem::path> pcd_files;
        std::cout << "Reading folder: " <<m_dataset_folder  << std::endl;
        if(!add_filename.empty()){
            add_file << "Reading folder: " <<m_dataset_folder << ".\n";
            std::cout << "Writing in file" << add_filename << std::endl;
        }
        // Set ROI
        m_sensor->select_roi(roi_x,roi_y);

        m_sensor->m_activate_roi = true;

        m_sensor->bundle_adjustment_parameter_changed(ba_params);
        m_sensor->cam_params_changed(cam_params_fixed);
        m_sensor->cam_tracker_params_changed(cam_tracker_params);
        m_sensor->set_roi_params(1, 0.75, 0.01); // (x , y percent of area to be considered,     offset above the plane)

        // Camera Start
        m_sensor->startTracker(0);
        if (boost::filesystem::exists (m_dataset_folder)) {
            boost::filesystem::directory_iterator end_itr;
            for (boost::filesystem::directory_iterator itr (m_dataset_folder); itr != end_itr; ++itr) {
    #if BOOST_FILESYSTEM_VERSION == 3
                if (!is_directory (itr->status ()) && boost::algorithm::to_upper_copy (boost::filesystem::extension (itr->path ())) == ".PCD" )
    #else
                if (!is_directory (itr->status ()) && boost::algorithm::to_upper_copy (boost::filesystem::extension (itr->leaf ())) == ".PCD" )
    #endif
                {
    #if BOOST_FILESYSTEM_VERSION == 3
                    pcd_files.push_back (itr->path ());
                    std::cout << "added: " << itr->path ().string () << std::endl;
    #else
                    pcd_files.push_back (itr->path ());
          std::cout << "added: " << itr->path () << std::endl;
    #endif
                }
            }
        } else {
            std::cout << "Folder " << m_dataset_folder << " doesn't exists" << std::endl;
            return -1;
        }
        // Sort the read files by name
        std::cout << "Sorting files" << std::endl;
        sort(pcd_files.begin (), pcd_files.end ());

        // Tracker start
        //for(int i = 0; i < 55; i++){
        for(int i = 0; i < pcd_files.size(); i++){
            std::cout << "Reading " << pcd_files[i].string() << std::endl;
            m_sensor->cloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>);
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
            pcl::io::loadPCDFile<pcl::PointXYZRGB>(pcd_files[i].string(), *cloud);
            m_sensor->cloud = cloud;
            v4r::convertCloud(*cloud, kp_cloud, image);
            std::cout << "kp_cloud.cols " << kp_cloud.cols << "kp_cloud.rows" << kp_cloud.rows << std::endl;

            // select a roi
            if (m_sensor->m_select_roi) {
                m_sensor->detectROI(kp_cloud);
                // emit doesnt work
                m_segmentation->set_roi(m_sensor->bb_min, m_sensor->bb_max, m_sensor->bbox_base_transform);
            }
            if (m_sensor->m_activate_roi) m_sensor->maskCloud(pose*m_sensor->bbox_base_transform, m_sensor->bb_min, m_sensor->bb_max, kp_cloud);

            // track camera
            if (m_run_tracker) {
              is_conf = m_sensor->camtracker->track(image, kp_cloud, pose, m_sensor->conf, m_sensor->cam_id);
              std::cout << "Confidence(" << is_conf << "): " << m_sensor->conf << std::endl;
              if (is_conf) {
                type = m_sensor->selectFrames(*cloud, m_sensor->cam_id, pose, *(m_sensor->cam_trajectory));

                if(type == 2 ){
                    std::cout << "Added: " << pcd_files[i].string()  << std::endl;

                    if(!add_filename.empty()){
                        add_file << "Added: " << pcd_files[i].string() << ".\n";
                        std::cout << "Wroton on filwwww" << std::endl;
                    }
                }
                if(type == 2 && use_custom_mask){
                    std::cout << "CUSTOM MASK" << std::endl;
                    std::string mask_filename = mask_root[j]+pcd_files[i].stem().string() + "/mask_" + class_name+ ".png";
                    std::cout << " maskfilename" << mask_filename<< std::endl;
                    cv::Mat_<unsigned char> new_mask;
                    new_mask= cv::imread(mask_filename, CV_LOAD_IMAGE_GRAYSCALE);
                    if(!new_mask.data){
                        std::cout << "Coulnd't read file " << mask_filename << std::endl;
                        return -1;
                    }
                    //cv::imshow("New Mask", new_mask);
                    //cv::waitKey(0);
                    masks.push_back(new_mask);

                }

                //if (type >= 0) m_ba->update_cam_trajectory(m_sensor->cam_trajectory);
                //if (type == 2 && m_sensor->cam_tracker_params.create_prev_cloud) m_ba->update_model_cloud(m_sensor->oc_cloud);
                //if (m_sensor->m_activate_roi) emit update_boundingbox(edges, pose*bbox_base_transform);
              }
            }

            if (m_sensor->m_draw_mask) m_sensor->drawDepthMask(*cloud,image);
            m_sensor->drawConfidenceBar(image,m_sensor->conf);
            /*cv::imshow("Image", image);
            cv::waitKey(1);*/
        }

        // Pose Optimize
        std::cout << "optimizeCamStructProj " << std::endl;
        m_ba->optimizeCamStructProj(m_sensor->getModel(),m_sensor->getTrajectory(),m_sensor->getClouds(),m_sensor->getAlignedCloud());

        // Object Segment
        std::cout << "Status: Postprocessing the object..." << std::endl;
        int idx_seg = 0;
        int num_clouds = m_sensor->getClouds()->size();
        std::cout << "Will optimize " << num_clouds << " clouds and"  << m_sensor->getCameras().size() << " cameras "<< std::endl;
        if(use_custom_mask)
            m_segmentation->setData(m_sensor->getCameras(), m_sensor->getClouds(), masks );
        else
            m_segmentation->setData(m_sensor->getCameras(), m_sensor->getClouds());


        //Object Ok
        //m_segmentation->finishSegmentation();
        m_segmentation->cmd = ObjectSegmentation::FINISH_OBJECT_MODELLING;
        m_segmentation->run();

        // Object Optimize
        if (!m_segmentation->optimizeMultiview()) {
          std::cout << "finishedObjectSegmentation()" << std::endl;
        }

        if (!m_segmentation->optimizeMultiview()) {
            m_segmentation->drawObjectCloud();
            m_sensor->showDepthMask(true);

        }

        std::cout << "Status: Finised segmentation" << std::endl;
        std::cout << "m_segmentation->getCameras().size()" << m_segmentation->getCameras().size() << "m_segmentation->getObjectIndices().size()" << m_segmentation->getObjectIndices().size()<< std::endl;

        // on_SessionAdd_clicked
        if (m_segmentation->getCameras().size()==0 || m_segmentation->getObjectIndices().size()==0) {
            std::cout << "No data available! Did you click 'Segment'?" << std::endl;
        }

        m_multi_session->addSequences(m_segmentation->getCameras(),m_sensor->getClouds(),m_segmentation->getObjectIndices(), m_segmentation->getObjectBaseTransform());

        // Sesion Align
        if(!m_first_iter){
            //m_multi_session->alignSequences();
            m_multi_session->cmd = MultiSession::MULTI_SESSION_ALIGNMENT;
            m_multi_session->run();
            // Session Optimize
            if (have_multi_session) {
                //m_multi_session->optimizeSequences();
                m_multi_session->cmd = MultiSession::MULTI_SESSION_MULTI_VIEW;
                m_multi_session->run();
            }

        }
        // Tracker Reset
        m_sensor->reset();
        m_first_iter = false;

    }



    bool ok_save = true;


    if ( ok_save && model_name.empty() == false ) {
      if (boost::filesystem::exists( model_folder + "/" + model_name + "/views/") ) {
        ok_save = false;
      }

        std::cout << "have_multi_session " << have_multi_session << std::endl;
        std::cout << "Ok_save in " << model_name<< std::endl;
        std::cout << "Ok_save " << ok_save<< std::endl;

        if (ok_save) {
        bool ok=false;
        if (have_multi_session)
          ok = m_multi_session->savePointClouds(model_folder, model_name);
        else
          ok = m_segmentation->savePointClouds(model_folder, model_name);

        if (ok) std::cout << "Status: Saved point clouds (recognition model)" << std::endl;
        else std::cout << "Status: No segmented data available!" << std::endl;
      }
    }


    std::cout << "Hello world" << std::endl;

    if(!add_filename.empty()){
        std::cout << "Closong file" << add_filename << std::endl;
        add_file.close();
    }
}

