#ifndef CONTROLS_H
#define CONTROLS_H

#include <QDialog>

#include <iostream>

namespace Ui {
class ControlsUi;
}

class Controls : public QDialog
{
    Q_OBJECT
	private slots:
		void on_CamStart_clicked();

	public:
		explicit Controls(QWidget *parent = 0);
		~Controls();

	public:
		Ui::ControlsUi *ui;
};

#endif // CONTROLS_H
