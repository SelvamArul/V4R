#include <v4r/common/miscellaneous.h>  // to extract Pose intrinsically stored in pcd file

#include <v4r/io/filesystem.h>
#include <v4r/recognition/multi_pipeline_recognizer.h>

#include <pcl/common/time.h>
#include <pcl/filters/passthrough.h>

#include <iostream>
#include <sstream>

#include <boost/any.hpp>
#include <boost/program_options.hpp>
#include <glog/logging.h>

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/features2d.hpp"


namespace po = boost::program_options;
using namespace cv;

int
main (int argc, char ** argv)
{
    typedef pcl::PointXYZRGB PointT;
    typedef v4r::Model<PointT> ModelT;
    typedef boost::shared_ptr<ModelT> ModelTPtr;

    typedef pcl::PointXYZRGB PointT;
    std::string test_dir;
    bool visualize = false;
    double chop_z = std::numeric_limits<double>::max();
    std::vector<boost::filesystem::path> pcd_files;

    double max_dist = 0; double min_dist = 100;



    std::string m_dataset_folder = argv[1];
    std::cout << "looking into folders " << m_dataset_folder << std::endl;
    if (boost::filesystem::exists (m_dataset_folder)) {
        boost::filesystem::directory_iterator end_itr;
        for (boost::filesystem::directory_iterator itr (m_dataset_folder); itr != end_itr; ++itr) {
#if BOOST_FILESYSTEM_VERSION == 3
            if (!is_directory (itr->status ()) && boost::algorithm::to_upper_copy (boost::filesystem::extension (itr->path ())) == ".JPG" )
#else
            if (!is_directory (itr->status ()) && boost::algorithm::to_upper_copy (boost::filesystem::extension (itr->leaf ())) == ".JPG" )
#endif
            {
#if BOOST_FILESYSTEM_VERSION == 3
                pcd_files.push_back (itr->path ());
                std::cout << "added: " << itr->path ().string () << std::endl;
#else
                pcd_files.push_back (itr->path ());
      std::cout << "added: " << itr->path () << std::endl;
#endif
            }
        }
    }

    cv::SiftFeatureDetector detector(40000);
    cv::SiftDescriptorExtractor extractor;


    cv::Mat descriptors_1;
    std::vector<KeyPoint> keypoints_1;
    std::string image_name = argv[2];
    std::cout << "Comparing with input image" << image_name<< std::endl;
    cv::Mat img_1= cv::imread(image_name, CV_LOAD_IMAGE_COLOR );
    detector.detect( img_1, keypoints_1);
    std::cout << "blahh"<<keypoints_1.size()<< std::endl;
    extractor.compute( img_1, keypoints_1, descriptors_1 );
    cv::Mat img_keypoints_1;
    drawKeypoints( img_1, keypoints_1, img_keypoints_1, Scalar::all(-1), DrawMatchesFlags::DEFAULT );
    imshow("Original matches",img_keypoints_1);
    cv::waitKey(0);

    for(int i = 0 ; i < pcd_files.size(); i++){

        std::string mask_name = pcd_files[i].string();
        boost::replace_last(mask_name, ".jpg", ".png");
        boost::replace_last(mask_name, "image", "cloud");


        cv::Mat descriptors_2;
        std::vector<KeyPoint> keypoints_2;

        cv::Mat img_2= imread(pcd_files[i].string(), CV_LOAD_IMAGE_COLOR);
        cv::Mat mask= imread(mask_name, CV_LOAD_IMAGE_ANYCOLOR);



        detector.detect( img_2, keypoints_2, mask);

        extractor.compute( img_2, keypoints_2, descriptors_2 );

        cv::Mat img_keypoints_2;

        drawKeypoints( img_2, keypoints_2, img_keypoints_2, Scalar::all(-1), DrawMatchesFlags::DEFAULT );
        imshow("Training image",img_keypoints_2);
        cv::waitKey(0);


        FlannBasedMatcher matcher;
         std::vector< DMatch > matches;
         matcher.match( descriptors_1, descriptors_2, matches );

         //-- Quick calculation of max and min distances between keypoints
           for( int i = 0; i < descriptors_1.rows; i++ )
           { double dist = matches[i].distance;
             if( dist < min_dist ) min_dist = dist;
             if( dist > max_dist ) max_dist = dist;
           }

           printf("-- Max dist : %f \n", max_dist );
           printf("-- Min dist : %f \n", min_dist );

           //-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist,
           //-- or a small arbitary value ( 0.02 ) in the event that min_dist is very
           //-- small)
           //-- PS.- radiusMatch can also be used here.
           std::vector< DMatch > good_matches;

           for( int i = 0; i < descriptors_1.rows; i++ )
           { if( matches[i].distance <= max(2*min_dist, 0.02) )
             { good_matches.push_back( matches[i]); }
           }

           //-- Draw only "good" matches
           Mat img_matches;
           drawMatches( img_1, keypoints_1, img_2, keypoints_2,
                        good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                        vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

           //-- Show detected matches
           imshow( "Good Matches", img_matches );
           waitKey(0);

           for( int i = 0; i < (int)good_matches.size(); i++ )
           { printf( "-- Good Match [%d] Keypoint 1: %d  -- Keypoint 2: %d  \n", i, good_matches[i].queryIdx, good_matches[i].trainIdx ); }
    }


    google::InitGoogleLogging(argv[0]);

    po::options_description desc("Single-View Object Instance Recognizer\n======================================\n**Allowed options");
    desc.add_options()
        ("help,h", "produce help message")
        ("test_dir,t", po::value<std::string>(&test_dir)->required(), "Directory with test scenes stored as point clouds (.pcd). The camera pose is taken directly from the pcd header fields \"sensor_orientation_\" and \"sensor_origin_\" (if the test directory contains subdirectories, each subdirectory is considered as seperate sequence for multiview recognition)")
        ("visualize,v", po::bool_switch(&visualize), "visualize recognition results")
        ("chop_z,z", po::value<double>(&chop_z)->default_value(chop_z, boost::str(boost::format("%.2e") % chop_z) ), "points with z-component higher than chop_z_ will be ignored (low chop_z reduces computation time and false positives (noise increase with z)")
   ;
    po::variables_map vm;
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(desc).allow_unregistered().run();
    po::store(parsed, vm);
    if (vm.count("help")) { std::cout << desc << std::endl; }
    try { po::notify(vm); }
    catch(std::exception& e) { std::cerr << "Error: " << e.what() << std::endl << std::endl << desc << std::endl;  }

    v4r::MultiRecognitionPipeline<PointT> r(argc, argv);

    // ----------- TEST ----------
    std::vector< std::string> sub_folder_names = v4r::io::getFoldersInDirectory( test_dir );
    if(sub_folder_names.empty())
        sub_folder_names.push_back("");

    for (const std::string &sub_folder_name : sub_folder_names)
    {
        const std::string sequence_path = test_dir + "/" + sub_folder_name;

        std::vector< std::string > views = v4r::io::getFilesInDirectory(sequence_path, ".*.pcd", false);
        for (size_t v_id=0; v_id<views.size(); v_id++)
        {
            const std::string fn = sequence_path + "/" + views[ v_id ];

            LOG(INFO) << "Recognizing file " << fn;
            typename pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>());
            pcl::io::loadPCDFile(fn, *cloud);

            if( chop_z > 0)
            {
                pcl::PassThrough<PointT> pass;
                pass.setFilterLimits ( 0.f, chop_z );
                pass.setFilterFieldName ("z");
                pass.setInputCloud (cloud);
                pass.setKeepOrganized (true);
                pass.filter (*cloud);
            }

            r.setInputCloud (cloud);
            r.recognize();

            std::vector<ModelTPtr> verified_models = r.getVerifiedModels();
            std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > transforms_verified;
            transforms_verified = r.getVerifiedTransforms();

            if (visualize)
                r.visualize();

            for(size_t m_id=0; m_id<verified_models.size(); m_id++)
                LOG(INFO) << "********************" << verified_models[m_id]->id_ << std::endl;
        }
    }
}
