# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/local/munoza/software/v4r_final/modules/common/src/ClusteringRNN.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/ClusteringRNN.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/common/src/ZAdaptiveNormals.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/ZAdaptiveNormals.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/common/src/color_transforms.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/color_transforms.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/common/src/geometric_consistency.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/geometric_consistency.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/common/src/graph_geometric_consistency.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/graph_geometric_consistency.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/common/src/kmeans.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/kmeans.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/common/src/miscellaneous.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/miscellaneous.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/common/src/noise_models.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/noise_models.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/common/src/organized_edge_detection.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/organized_edge_detection.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/common/src/pcl_visualization_utils.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/pcl_visualization_utils.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/common/src/visibility_reasoning.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/visibility_reasoning.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/common/src/zbuffering.cpp" "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common_object.dir/src/zbuffering.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "EIGEN_NO_DEBUG"
  "V4RAPI_EXPORTS"
  "__V4R_BUILD=1"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "/usr/local/include/eigen3"
  "/usr/include/vtk-5.8"
  "/usr/include/opencv"
  "../modules/common/include"
  "../modules/common/src"
  "modules/common"
  "../modules/core/include"
  "/usr/include/pcl-1.7"
  "/usr/include/ni"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
