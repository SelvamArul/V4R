# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/local/munoza/software/v4r_final/modules/io/src/eigen.cpp" "/home/local/munoza/software/v4r_final/build/modules/io/CMakeFiles/v4r_io.dir/src/eigen.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/io/src/filesystem.cpp" "/home/local/munoza/software/v4r_final/build/modules/io/CMakeFiles/v4r_io.dir/src/filesystem.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "EIGEN_NO_DEBUG"
  "V4RAPI_EXPORTS"
  "__V4R_BUILD=1"
  )

# Pairs of files generated by the same build rule.
SET(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/home/local/munoza/software/v4r_final/build/lib/libv4r_io.so" "/home/local/munoza/software/v4r_final/build/lib/libv4r_io.so.1.0.0"
  "/home/local/munoza/software/v4r_final/build/lib/libv4r_io.so.1.0" "/home/local/munoza/software/v4r_final/build/lib/libv4r_io.so.1.0.0"
  )


# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/local/munoza/software/v4r_final/build/modules/core/CMakeFiles/v4r_core.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "/usr/local/include/eigen3"
  "/usr/include/vtk-5.8"
  "/usr/include/opencv"
  "../modules/io/include"
  "../modules/io/src"
  "modules/io"
  "../modules/core/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
