# Install script for directory: /home/local/munoza/software/v4r_final/modules

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/common/.common/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/core/.core/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/features/.features/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/io/.io/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/keypoints/.keypoints/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/ml/.ml/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/object_modelling/.object_modelling/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/recognition/.recognition/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/reconstruction/.reconstruction/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/registration/.registration/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/rendering/.rendering/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/segmentation/.segmentation/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/tracking/.tracking/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/core/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/io/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/common/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/features/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/keypoints/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/ml/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/reconstruction/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/registration/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/rendering/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/segmentation/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/tracking/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/object_modelling/cmake_install.cmake")
  INCLUDE("/home/local/munoza/software/v4r_final/build/modules/recognition/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

