# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/local/munoza/software/v4r_final/modules/rendering/src/depthmapRenderer.cpp" "/home/local/munoza/software/v4r_final/build/modules/rendering/CMakeFiles/v4r_rendering_object.dir/src/depthmapRenderer.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/rendering/src/dmRenderObject.cpp" "/home/local/munoza/software/v4r_final/build/modules/rendering/CMakeFiles/v4r_rendering_object.dir/src/dmRenderObject.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "EIGEN_NO_DEBUG"
  "V4RAPI_EXPORTS"
  "__V4R_BUILD=1"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "/usr/local/include/eigen3"
  "/usr/include/vtk-5.8"
  "/usr/include/opencv"
  "../modules/rendering/include"
  "../modules/rendering/src"
  "modules/rendering"
  "../modules/core/include"
  "../modules/common/include"
  "/usr/include/glm"
  "/usr/include/pcl-1.7"
  "/usr/include/ni"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
