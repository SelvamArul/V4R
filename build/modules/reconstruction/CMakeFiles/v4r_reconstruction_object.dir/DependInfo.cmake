# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/KeyframeManagementRGBD2.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/KeyframeManagementRGBD2.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/KeypointPoseDetector.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/KeypointPoseDetector.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/KeypointPoseDetectorRT.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/KeypointPoseDetectorRT.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/KeypointSlamRGBD2.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/KeypointSlamRGBD2.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/LKPoseTracker.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/LKPoseTracker.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/LKPoseTrackerRT.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/LKPoseTrackerRT.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/ProjBundleAdjuster.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/ProjBundleAdjuster.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/ProjLKPoseTrackerLM.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/ProjLKPoseTrackerLM.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/ProjLKPoseTrackerR2.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/ProjLKPoseTrackerR2.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/ProjLKPoseTrackerRT.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/ProjLKPoseTrackerRT.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/RefinePatchLocationLK.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/RefinePatchLocationLK.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/reconstruction/src/RefineProjectedPointLocationLK.cpp" "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction_object.dir/src/RefineProjectedPointLocationLK.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "EIGEN_NO_DEBUG"
  "V4RAPI_EXPORTS"
  "__V4R_BUILD=1"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "/usr/local/include/eigen3"
  "/usr/include/vtk-5.8"
  "/usr/include/opencv"
  "../modules/reconstruction/include"
  "../modules/reconstruction/src"
  "modules/reconstruction"
  "../modules/core/include"
  "../modules/io/include"
  "../modules/common/include"
  "../modules/features/include"
  "../modules/keypoints/include"
  "/home/local/munoza/lib/usr/local/include"
  "/usr/include/pcl-1.7"
  "/usr/include/ni"
  "../3rdparty/SiftGPU/src/siftgpu_external/src"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
