# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/local/munoza/software/v4r_final/modules/object_modelling/src/incremental_object_learning.cpp" "/home/local/munoza/software/v4r_final/build/modules/object_modelling/CMakeFiles/v4r_object_modelling_object.dir/src/incremental_object_learning.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/object_modelling/src/visualization.cpp" "/home/local/munoza/software/v4r_final/build/modules/object_modelling/CMakeFiles/v4r_object_modelling_object.dir/src/visualization.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "EIGEN_NO_DEBUG"
  "V4RAPI_EXPORTS"
  "__V4R_BUILD=1"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "/usr/local/include/eigen3"
  "/usr/include/vtk-5.8"
  "/usr/include/opencv"
  "../modules/object_modelling/include"
  "../modules/object_modelling/src"
  "modules/object_modelling"
  "../modules/core/include"
  "../modules/io/include"
  "../modules/common/include"
  "../modules/features/include"
  "../modules/keypoints/include"
  "../modules/registration/include"
  "/home/local/munoza/lib/usr/local/include"
  "../3rdparty"
  "/usr/include/pcl-1.7"
  "/usr/include/ni"
  "../3rdparty/SiftGPU/src/siftgpu_external/src"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
