# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/local/munoza/software/v4r_final/modules/features/src/ComputeImGDescOrientations.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/ComputeImGDescOrientations.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/ComputeImGradientDescriptors.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/ComputeImGradientDescriptors.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/FeatureDetector_D_FREAK.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/FeatureDetector_D_FREAK.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/FeatureDetector_KD_FAST_IMGD.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/FeatureDetector_KD_FAST_IMGD.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/FeatureDetector_KD_ORB.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/FeatureDetector_KD_ORB.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/FeatureDetector_K_HARRIS.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/FeatureDetector_K_HARRIS.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/FeatureSelection.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/FeatureSelection.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/ImGDescOrientation.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/ImGDescOrientation.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/ImGradientDescriptor.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/ImGradientDescriptor.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/esf_estimator.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/esf_estimator.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/keypoint_extractor.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/keypoint_extractor.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/local_estimator.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/local_estimator.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/opencv_sift_local_estimator.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/opencv_sift_local_estimator.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/shot_local_estimator.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/shot_local_estimator.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/shot_local_estimator_omp.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/shot_local_estimator_omp.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/sift_local_estimator.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/sift_local_estimator.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/uniform_sampling.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/uniform_sampling.cpp.o"
  "/home/local/munoza/software/v4r_final/modules/features/src/uniform_sampling_extractor.cpp" "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features_object.dir/src/uniform_sampling_extractor.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "EIGEN_NO_DEBUG"
  "V4RAPI_EXPORTS"
  "__V4R_BUILD=1"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "/usr/local/include/eigen3"
  "/usr/include/vtk-5.8"
  "/usr/include/opencv"
  "../modules/features/include"
  "../modules/features/src"
  "modules/features"
  "../modules/core/include"
  "../modules/common/include"
  "/usr/include/pcl-1.7"
  "/usr/include/ni"
  "../3rdparty/SiftGPU/src/siftgpu_external/src"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
