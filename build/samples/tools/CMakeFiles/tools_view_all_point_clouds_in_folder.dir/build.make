# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/local/munoza/software/v4r_final

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/local/munoza/software/v4r_final/build

# Include any dependencies generated for this target.
include samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/depend.make

# Include the progress variables for this target.
include samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/progress.make

# Include the compile flags for this target's objects.
include samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/flags.make

samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o: samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/flags.make
samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o: ../samples/tools/view_all_point_clouds_in_folder.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/local/munoza/software/v4r_final/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o"
	cd /home/local/munoza/software/v4r_final/build/samples/tools && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o -c /home/local/munoza/software/v4r_final/samples/tools/view_all_point_clouds_in_folder.cpp

samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.i"
	cd /home/local/munoza/software/v4r_final/build/samples/tools && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/local/munoza/software/v4r_final/samples/tools/view_all_point_clouds_in_folder.cpp > CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.i

samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.s"
	cd /home/local/munoza/software/v4r_final/build/samples/tools && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/local/munoza/software/v4r_final/samples/tools/view_all_point_clouds_in_folder.cpp -o CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.s

samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o.requires:
.PHONY : samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o.requires

samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o.provides: samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o.requires
	$(MAKE) -f samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/build.make samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o.provides.build
.PHONY : samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o.provides

samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o.provides.build: samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o

# Object files for target tools_view_all_point_clouds_in_folder
tools_view_all_point_clouds_in_folder_OBJECTS = \
"CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o"

# External object files for target tools_view_all_point_clouds_in_folder
tools_view_all_point_clouds_in_folder_EXTERNAL_OBJECTS =

bin/tools-view_all_point_clouds_in_folder: samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o
bin/tools-view_all_point_clouds_in_folder: samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/build.make
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_common.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_core.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_features.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_io.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_keypoints.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_ml.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_recognition.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_reconstruction.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_registration.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_rendering.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_segmentation.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_tracking.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_system.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_thread.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_date_time.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_iostreams.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libpthread.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_common.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_octree.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libOpenNI.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkCommon.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkRendering.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkHybrid.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkCharts.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_io.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libflann_cpp_s.a
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_kdtree.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_search.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_sample_consensus.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_filters.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_features.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_keypoints.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_segmentation.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_visualization.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_outofcore.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_registration.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_recognition.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libqhull.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_surface.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_people.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_tracking.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_apps.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_system.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_thread.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_date_time.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_iostreams.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libpthread.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libqhull.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libOpenNI.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libflann_cpp_s.a
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkCommon.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkRendering.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkHybrid.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkCharts.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_videostab.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_ts.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_superres.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_stitching.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_ocl.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_nonfree.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_gpu.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_contrib.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: 3rdparty/lib/libedt.a
bin/tools-view_all_point_clouds_in_folder: /home/local/munoza/lib/usr/local/lib/libceres.a
bin/tools-view_all_point_clouds_in_folder: ../3rdparty/libsvm/src/libsvm_external/libsvm.so.2
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libGLU.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libGL.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libSM.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libICE.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libX11.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libXext.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libassimp.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libglog.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_thread.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_program_options.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_system.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_regex.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libpthread.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libGLEW.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libGLU.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libGL.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libSM.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libICE.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libX11.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libXext.so
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_reconstruction.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_keypoints.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_registration.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_features.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_common.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_io.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: lib/libv4r_core.so.1.0.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_videostab.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_ts.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_superres.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_stitching.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_contrib.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_nonfree.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_ocl.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_gpu.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_program_options.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_regex.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_octree.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_system.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_thread.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_date_time.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_iostreams.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_common.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libpthread.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libOpenNI.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkCharts.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkViews.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkInfovis.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkWidgets.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkHybrid.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkParallel.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkVolumeRendering.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkRendering.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkGraphics.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkImaging.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkIO.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkFiltering.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtkCommon.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libvtksys.so.5.8.0
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_io.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libflann_cpp_s.a
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_kdtree.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_search.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_sample_consensus.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_filters.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_features.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_keypoints.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_segmentation.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_visualization.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_outofcore.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_registration.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_recognition.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libqhull.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_surface.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_people.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_tracking.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libpcl_apps.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libspqr.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libtbb.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libtbbmalloc.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libcholmod.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libccolamd.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libcamd.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libcolamd.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libamd.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/liblapack.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libblas.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libsuitesparseconfig.a
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/librt.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/liblapack.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/libblas.so
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/libsuitesparseconfig.a
bin/tools-view_all_point_clouds_in_folder: /usr/lib/x86_64-linux-gnu/librt.so
bin/tools-view_all_point_clouds_in_folder: samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable ../../bin/tools-view_all_point_clouds_in_folder"
	cd /home/local/munoza/software/v4r_final/build/samples/tools && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/tools_view_all_point_clouds_in_folder.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/build: bin/tools-view_all_point_clouds_in_folder
.PHONY : samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/build

samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/requires: samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/view_all_point_clouds_in_folder.cpp.o.requires
.PHONY : samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/requires

samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/clean:
	cd /home/local/munoza/software/v4r_final/build/samples/tools && $(CMAKE_COMMAND) -P CMakeFiles/tools_view_all_point_clouds_in_folder.dir/cmake_clean.cmake
.PHONY : samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/clean

samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/depend:
	cd /home/local/munoza/software/v4r_final/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/local/munoza/software/v4r_final /home/local/munoza/software/v4r_final/samples/tools /home/local/munoza/software/v4r_final/build /home/local/munoza/software/v4r_final/build/samples/tools /home/local/munoza/software/v4r_final/build/samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : samples/tools/CMakeFiles/tools_view_all_point_clouds_in_folder.dir/depend

