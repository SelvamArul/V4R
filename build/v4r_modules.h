/*
 *      ** File generated automatically, do not modify **
 *
 * This file defines the list of modules available in current build configuration
 *
 *
*/

#define HAVE_V4R_COMMON
#define HAVE_V4R_CORE
#define HAVE_V4R_FEATURES
#define HAVE_V4R_IO
#define HAVE_V4R_KEYPOINTS
#define HAVE_V4R_ML
#define HAVE_V4R_OBJECT_MODELLING
#define HAVE_V4R_RECOGNITION
#define HAVE_V4R_RECONSTRUCTION
#define HAVE_V4R_REGISTRATION
#define HAVE_V4R_RENDERING
#define HAVE_V4R_SEGMENTATION
#define HAVE_V4R_TRACKING


