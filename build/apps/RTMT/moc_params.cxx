/****************************************************************************
** Meta object code from reading C++ file 'params.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../apps/RTMT/params.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'params.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Params[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,    8,    7,    7, 0x05,
      58,   52,    7,    7, 0x05,
     109,    7,    7,    7, 0x05,
     129,   52,    7,    7, 0x05,
     192,   52,    7,    7, 0x05,
     246,   52,    7,    7, 0x05,
     342,  298,    7,    7, 0x05,
     427,  379,    7,    7, 0x05,
     494,  476,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
     519,    7,    7,    7, 0x08,
     541,    7,    7,    7, 0x08,
     571,    7,    7,    7, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Params[] = {
    "Params\0\0cam\0cam_params_changed(RGBDCameraParameter)\0"
    "param\0cam_tracker_params_changed(CamaraTrackerParameter)\0"
    "rgbd_path_changed()\0"
    "bundle_adjustment_parameter_changed(BundleAdjustmentParameter)\0"
    "segmentation_parameter_changed(SegmentationParameter)\0"
    "object_modelling_parameter_changed(ObjectModelling)\0"
    "_bbox_scale_xy,_bbox_scale_height,_seg_offs\0"
    "set_roi_params(double,double,double)\0"
    "use_roi_segm,offs,_use_dense_mv,_edge_radius_px\0"
    "set_segmentation_params(bool,double,bool,double)\0"
    "create_cb,rnn_thr\0set_cb_param(bool,float)\0"
    "on_okButton_clicked()\0"
    "on_pushFindRGBDPath_pressed()\0"
    "on_applyButton_clicked()\0"
};

void Params::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Params *_t = static_cast<Params *>(_o);
        switch (_id) {
        case 0: _t->cam_params_changed((*reinterpret_cast< const RGBDCameraParameter(*)>(_a[1]))); break;
        case 1: _t->cam_tracker_params_changed((*reinterpret_cast< const CamaraTrackerParameter(*)>(_a[1]))); break;
        case 2: _t->rgbd_path_changed(); break;
        case 3: _t->bundle_adjustment_parameter_changed((*reinterpret_cast< const BundleAdjustmentParameter(*)>(_a[1]))); break;
        case 4: _t->segmentation_parameter_changed((*reinterpret_cast< const SegmentationParameter(*)>(_a[1]))); break;
        case 5: _t->object_modelling_parameter_changed((*reinterpret_cast< const ObjectModelling(*)>(_a[1]))); break;
        case 6: _t->set_roi_params((*reinterpret_cast< const double(*)>(_a[1])),(*reinterpret_cast< const double(*)>(_a[2])),(*reinterpret_cast< const double(*)>(_a[3]))); break;
        case 7: _t->set_segmentation_params((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< const double(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3])),(*reinterpret_cast< const double(*)>(_a[4]))); break;
        case 8: _t->set_cb_param((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 9: _t->on_okButton_clicked(); break;
        case 10: _t->on_pushFindRGBDPath_pressed(); break;
        case 11: _t->on_applyButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Params::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Params::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Params,
      qt_meta_data_Params, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Params::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Params::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Params::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Params))
        return static_cast<void*>(const_cast< Params*>(this));
    return QDialog::qt_metacast(_clname);
}

int Params::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void Params::cam_params_changed(const RGBDCameraParameter & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Params::cam_tracker_params_changed(const CamaraTrackerParameter & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Params::rgbd_path_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void Params::bundle_adjustment_parameter_changed(const BundleAdjustmentParameter & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Params::segmentation_parameter_changed(const SegmentationParameter & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Params::object_modelling_parameter_changed(const ObjectModelling & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Params::set_roi_params(const double & _t1, const double & _t2, const double & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Params::set_segmentation_params(bool _t1, const double & _t2, bool _t3, const double & _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Params::set_cb_param(bool _t1, float _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_END_MOC_NAMESPACE
