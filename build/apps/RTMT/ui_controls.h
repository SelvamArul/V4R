/********************************************************************************
** Form generated from reading UI file 'controls.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONTROLS_H
#define UI_CONTROLS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ControlsUi
{
public:
    QWidget *layoutWidget;
    QGridLayout *gridLayout_4;
    QLabel *label_8;
    QPushButton *CamStart;
    QPushButton *CamStop;
    QLabel *label_10;
    QPushButton *setROI;
    QPushButton *ActivateROI;
    QPushButton *ResetROI;
    QLabel *label_11;
    QPushButton *TrackerStart;
    QPushButton *TrackerStop;
    QPushButton *ResetTracker;
    QWidget *layoutWidget_2;
    QGridLayout *gridLayout_5;
    QLabel *label_12;
    QPushButton *OptimizePoses;
    QCheckBox *ShowPointCloud;
    QPushButton *undoOptimize;
    QCheckBox *ShowObjectModel;
    QLabel *label_13;
    QPushButton *SegmentObject;
    QPushButton *okSegmentation;
    QPushButton *OptimizeObject;
    QCheckBox *ShowCameras;
    QWidget *layoutWidget_3;
    QGridLayout *gridLayout_6;
    QPushButton *SessionClear;
    QLabel *label_14;
    QPushButton *SessionAdd;
    QPushButton *SessionAlign;
    QPushButton *ResetView_2;
    QPushButton *SaveTrackerModel;
    QPushButton *SavePointClouds;
    QLabel *label_15;
    QPushButton *SessionOptimize;
    QLabel *label_16;
    QLineEdit *PCDFolder;
    QLabel *statusLabel;
    QWidget *layoutWidget5;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *ShowImage;
    QCheckBox *ShowDepthMask;
    QPushButton *imBackward;
    QPushButton *imForward;

    void setupUi(QDialog *ControlsUi)
    {
        if (ControlsUi->objectName().isEmpty())
            ControlsUi->setObjectName(QString::fromUtf8("ControlsUi"));
        ControlsUi->resize(586, 356);
        layoutWidget = new QWidget(ControlsUi);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(30, 20, 333, 95));
        gridLayout_4 = new QGridLayout(layoutWidget);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_4->addWidget(label_8, 0, 0, 1, 1);

        CamStart = new QPushButton(layoutWidget);
        CamStart->setObjectName(QString::fromUtf8("CamStart"));

        gridLayout_4->addWidget(CamStart, 0, 1, 1, 1);

        CamStop = new QPushButton(layoutWidget);
        CamStop->setObjectName(QString::fromUtf8("CamStop"));

        gridLayout_4->addWidget(CamStop, 0, 2, 1, 1);

        label_10 = new QLabel(layoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_4->addWidget(label_10, 1, 0, 1, 1);

        setROI = new QPushButton(layoutWidget);
        setROI->setObjectName(QString::fromUtf8("setROI"));

        gridLayout_4->addWidget(setROI, 1, 1, 1, 1);

        ActivateROI = new QPushButton(layoutWidget);
        ActivateROI->setObjectName(QString::fromUtf8("ActivateROI"));

        gridLayout_4->addWidget(ActivateROI, 1, 2, 1, 1);

        ResetROI = new QPushButton(layoutWidget);
        ResetROI->setObjectName(QString::fromUtf8("ResetROI"));

        gridLayout_4->addWidget(ResetROI, 1, 3, 1, 1);

        label_11 = new QLabel(layoutWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_4->addWidget(label_11, 2, 0, 1, 1);

        TrackerStart = new QPushButton(layoutWidget);
        TrackerStart->setObjectName(QString::fromUtf8("TrackerStart"));

        gridLayout_4->addWidget(TrackerStart, 2, 1, 1, 1);

        TrackerStop = new QPushButton(layoutWidget);
        TrackerStop->setObjectName(QString::fromUtf8("TrackerStop"));

        gridLayout_4->addWidget(TrackerStop, 2, 2, 1, 1);

        ResetTracker = new QPushButton(layoutWidget);
        ResetTracker->setObjectName(QString::fromUtf8("ResetTracker"));

        gridLayout_4->addWidget(ResetTracker, 2, 3, 1, 1);

        layoutWidget_2 = new QWidget(ControlsUi);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(30, 130, 521, 62));
        gridLayout_5 = new QGridLayout(layoutWidget_2);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(0, 0, 0, 0);
        label_12 = new QLabel(layoutWidget_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout_5->addWidget(label_12, 0, 0, 1, 1);

        OptimizePoses = new QPushButton(layoutWidget_2);
        OptimizePoses->setObjectName(QString::fromUtf8("OptimizePoses"));
        OptimizePoses->setEnabled(true);

        gridLayout_5->addWidget(OptimizePoses, 0, 1, 1, 1);

        ShowPointCloud = new QCheckBox(layoutWidget_2);
        ShowPointCloud->setObjectName(QString::fromUtf8("ShowPointCloud"));
        ShowPointCloud->setEnabled(true);
        ShowPointCloud->setChecked(false);

        gridLayout_5->addWidget(ShowPointCloud, 0, 3, 1, 1);

        undoOptimize = new QPushButton(layoutWidget_2);
        undoOptimize->setObjectName(QString::fromUtf8("undoOptimize"));
        undoOptimize->setEnabled(true);

        gridLayout_5->addWidget(undoOptimize, 0, 2, 1, 1);

        ShowObjectModel = new QCheckBox(layoutWidget_2);
        ShowObjectModel->setObjectName(QString::fromUtf8("ShowObjectModel"));
        ShowObjectModel->setEnabled(true);
        ShowObjectModel->setChecked(false);

        gridLayout_5->addWidget(ShowObjectModel, 0, 4, 1, 1);

        label_13 = new QLabel(layoutWidget_2);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_5->addWidget(label_13, 1, 0, 1, 1);

        SegmentObject = new QPushButton(layoutWidget_2);
        SegmentObject->setObjectName(QString::fromUtf8("SegmentObject"));

        gridLayout_5->addWidget(SegmentObject, 1, 1, 1, 1);

        okSegmentation = new QPushButton(layoutWidget_2);
        okSegmentation->setObjectName(QString::fromUtf8("okSegmentation"));
        okSegmentation->setEnabled(true);

        gridLayout_5->addWidget(okSegmentation, 1, 2, 1, 1);

        OptimizeObject = new QPushButton(layoutWidget_2);
        OptimizeObject->setObjectName(QString::fromUtf8("OptimizeObject"));
        OptimizeObject->setEnabled(true);

        gridLayout_5->addWidget(OptimizeObject, 1, 3, 1, 1);

        ShowCameras = new QCheckBox(layoutWidget_2);
        ShowCameras->setObjectName(QString::fromUtf8("ShowCameras"));

        gridLayout_5->addWidget(ShowCameras, 1, 4, 1, 1);

        layoutWidget_3 = new QWidget(ControlsUi);
        layoutWidget_3->setObjectName(QString::fromUtf8("layoutWidget_3"));
        layoutWidget_3->setGeometry(QRect(30, 200, 536, 62));
        gridLayout_6 = new QGridLayout(layoutWidget_3);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_6->setContentsMargins(0, 0, 0, 0);
        SessionClear = new QPushButton(layoutWidget_3);
        SessionClear->setObjectName(QString::fromUtf8("SessionClear"));

        gridLayout_6->addWidget(SessionClear, 2, 4, 1, 1);

        label_14 = new QLabel(layoutWidget_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_6->addWidget(label_14, 2, 0, 1, 1);

        SessionAdd = new QPushButton(layoutWidget_3);
        SessionAdd->setObjectName(QString::fromUtf8("SessionAdd"));

        gridLayout_6->addWidget(SessionAdd, 2, 1, 1, 1);

        SessionAlign = new QPushButton(layoutWidget_3);
        SessionAlign->setObjectName(QString::fromUtf8("SessionAlign"));

        gridLayout_6->addWidget(SessionAlign, 2, 2, 1, 1);

        ResetView_2 = new QPushButton(layoutWidget_3);
        ResetView_2->setObjectName(QString::fromUtf8("ResetView_2"));

        gridLayout_6->addWidget(ResetView_2, 2, 5, 1, 1);

        SaveTrackerModel = new QPushButton(layoutWidget_3);
        SaveTrackerModel->setObjectName(QString::fromUtf8("SaveTrackerModel"));
        SaveTrackerModel->setEnabled(true);

        gridLayout_6->addWidget(SaveTrackerModel, 0, 1, 1, 1);

        SavePointClouds = new QPushButton(layoutWidget_3);
        SavePointClouds->setObjectName(QString::fromUtf8("SavePointClouds"));

        gridLayout_6->addWidget(SavePointClouds, 0, 2, 1, 1);

        label_15 = new QLabel(layoutWidget_3);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_6->addWidget(label_15, 0, 0, 1, 1);

        SessionOptimize = new QPushButton(layoutWidget_3);
        SessionOptimize->setObjectName(QString::fromUtf8("SessionOptimize"));

        gridLayout_6->addWidget(SessionOptimize, 2, 3, 1, 1);

        label_16 = new QLabel(layoutWidget_3);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout_6->addWidget(label_16, 0, 3, 1, 1);

        PCDFolder = new QLineEdit(layoutWidget_3);
        PCDFolder->setObjectName(QString::fromUtf8("PCDFolder"));

        gridLayout_6->addWidget(PCDFolder, 0, 4, 1, 2);

        statusLabel = new QLabel(ControlsUi);
        statusLabel->setObjectName(QString::fromUtf8("statusLabel"));
        statusLabel->setGeometry(QRect(40, 310, 331, 21));
        layoutWidget5 = new QWidget(ControlsUi);
        layoutWidget5->setObjectName(QString::fromUtf8("layoutWidget5"));
        layoutWidget5->setGeometry(QRect(370, 20, 178, 52));
        verticalLayout_4 = new QVBoxLayout(layoutWidget5);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        ShowImage = new QCheckBox(layoutWidget5);
        ShowImage->setObjectName(QString::fromUtf8("ShowImage"));
        ShowImage->setChecked(true);

        verticalLayout_4->addWidget(ShowImage);

        ShowDepthMask = new QCheckBox(layoutWidget5);
        ShowDepthMask->setObjectName(QString::fromUtf8("ShowDepthMask"));
        ShowDepthMask->setChecked(true);

        verticalLayout_4->addWidget(ShowDepthMask);

        imBackward = new QPushButton(ControlsUi);
        imBackward->setObjectName(QString::fromUtf8("imBackward"));
        imBackward->setEnabled(false);
        imBackward->setGeometry(QRect(30, 270, 94, 27));
        imForward = new QPushButton(ControlsUi);
        imForward->setObjectName(QString::fromUtf8("imForward"));
        imForward->setEnabled(false);
        imForward->setGeometry(QRect(130, 270, 94, 27));

        retranslateUi(ControlsUi);

        QMetaObject::connectSlotsByName(ControlsUi);
    } // setupUi

    void retranslateUi(QDialog *ControlsUi)
    {
        ControlsUi->setWindowTitle(QApplication::translate("ControlsUi", "Dialog", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("ControlsUi", "Camera", 0, QApplication::UnicodeUTF8));
        CamStart->setText(QApplication::translate("ControlsUi", "Start", 0, QApplication::UnicodeUTF8));
        CamStop->setText(QApplication::translate("ControlsUi", "Stop", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("ControlsUi", "ROI", 0, QApplication::UnicodeUTF8));
        setROI->setText(QApplication::translate("ControlsUi", "Set", 0, QApplication::UnicodeUTF8));
        ActivateROI->setText(QApplication::translate("ControlsUi", "Activate", 0, QApplication::UnicodeUTF8));
        ResetROI->setText(QApplication::translate("ControlsUi", "Reset", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("ControlsUi", "Tracker", 0, QApplication::UnicodeUTF8));
        TrackerStart->setText(QApplication::translate("ControlsUi", "Start", 0, QApplication::UnicodeUTF8));
        TrackerStop->setText(QApplication::translate("ControlsUi", "Stop", 0, QApplication::UnicodeUTF8));
        ResetTracker->setText(QApplication::translate("ControlsUi", "Reset", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("ControlsUi", "Pose", 0, QApplication::UnicodeUTF8));
        OptimizePoses->setText(QApplication::translate("ControlsUi", "Optimize", 0, QApplication::UnicodeUTF8));
        ShowPointCloud->setText(QApplication::translate("ControlsUi", "point cloud", 0, QApplication::UnicodeUTF8));
        undoOptimize->setText(QApplication::translate("ControlsUi", "... Undo", 0, QApplication::UnicodeUTF8));
        ShowObjectModel->setText(QApplication::translate("ControlsUi", "object model", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("ControlsUi", "Object", 0, QApplication::UnicodeUTF8));
        SegmentObject->setText(QApplication::translate("ControlsUi", "Segment", 0, QApplication::UnicodeUTF8));
        okSegmentation->setText(QApplication::translate("ControlsUi", "... Ok", 0, QApplication::UnicodeUTF8));
        OptimizeObject->setText(QApplication::translate("ControlsUi", "Optimize", 0, QApplication::UnicodeUTF8));
        ShowCameras->setText(QApplication::translate("ControlsUi", "camera trajectory", 0, QApplication::UnicodeUTF8));
        SessionClear->setText(QApplication::translate("ControlsUi", "Clear", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("ControlsUi", "Session", 0, QApplication::UnicodeUTF8));
        SessionAdd->setText(QApplication::translate("ControlsUi", "Add", 0, QApplication::UnicodeUTF8));
        SessionAlign->setText(QApplication::translate("ControlsUi", "Align", 0, QApplication::UnicodeUTF8));
        ResetView_2->setText(QApplication::translate("ControlsUi", "Reset View", 0, QApplication::UnicodeUTF8));
        SaveTrackerModel->setText(QApplication::translate("ControlsUi", "Tracker", 0, QApplication::UnicodeUTF8));
        SavePointClouds->setText(QApplication::translate("ControlsUi", "Recognizer", 0, QApplication::UnicodeUTF8));
        label_15->setText(QApplication::translate("ControlsUi", "Store for", 0, QApplication::UnicodeUTF8));
        SessionOptimize->setText(QApplication::translate("ControlsUi", "Optimize", 0, QApplication::UnicodeUTF8));
        label_16->setText(QApplication::translate("ControlsUi", "PCD folder", 0, QApplication::UnicodeUTF8));
        statusLabel->setText(QApplication::translate("ControlsUi", "Status: Ready", 0, QApplication::UnicodeUTF8));
        ShowImage->setText(QApplication::translate("ControlsUi", "image", 0, QApplication::UnicodeUTF8));
        ShowDepthMask->setText(QApplication::translate("ControlsUi", "mask missing depth", 0, QApplication::UnicodeUTF8));
        imBackward->setText(QApplication::translate("ControlsUi", "<", 0, QApplication::UnicodeUTF8));
        imForward->setText(QApplication::translate("ControlsUi", ">", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ControlsUi: public Ui_ControlsUi {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONTROLS_H
