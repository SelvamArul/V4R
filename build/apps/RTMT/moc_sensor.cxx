/****************************************************************************
** Meta object code from reading C++ file 'sensor.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../apps/RTMT/sensor.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sensor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Sensor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,    8,    7,    7, 0x05,
      97,   91,    7,    7, 0x05,
     133,  123,    7,    7, 0x05,
     221,  205,    7,    7, 0x05,
     300,    7,    7,    7, 0x05,
     328,  323,    7,    7, 0x05,
     365,  353,    7,    7, 0x05,
     405,  394,    7,    7, 0x05,
     496,  470,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
     565,  553,    7,    7, 0x0a,
     625,  605,    7,    7, 0x0a,
     682,  676,    7,    7, 0x0a,
     749,  745,    7,    7, 0x0a,
     813,  769,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Sensor[] = {
    "Sensor\0\0_cloud,image\0"
    "new_image(pcl::PointCloud<pcl::PointXYZRGB>::Ptr,cv::Mat_<cv::Vec3b>)\0"
    "_pose\0new_pose(Eigen::Matrix4f)\0"
    "_oc_cloud\0"
    "update_model_cloud(boost::shared_ptr<Sensor::AlignedPointXYZRGBVector>"
    ")\0"
    "_cam_trajectory\0"
    "update_cam_trajectory(boost::shared_ptr<std::vector<Sensor::CameraLoca"
    "tion> >)\0"
    "update_visualization()\0_txt\0"
    "printStatus(std::string)\0num_cameras\0"
    "finishedOptimizeCameras(int)\0edges,pose\0"
    "update_boundingbox(std::vector<Eigen::Vector3f>,Eigen::Matrix4f)\0"
    "_bb_min,_bb_max,_roi_pose\0"
    "set_roi(Eigen::Vector3f,Eigen::Vector3f,Eigen::Matrix4f)\0"
    "_cam_params\0cam_params_changed(RGBDCameraParameter)\0"
    "_cam_tracker_params\0"
    "cam_tracker_params_changed(CamaraTrackerParameter)\0"
    "param\0"
    "bundle_adjustment_parameter_changed(BundleAdjustmentParameter)\0"
    "x,y\0select_roi(int,int)\0"
    "_bbox_scale_xy,_bbox_scale_height,_seg_offs\0"
    "set_roi_params(double,double,double)\0"
};

void Sensor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Sensor *_t = static_cast<Sensor *>(_o);
        switch (_id) {
        case 0: _t->new_image((*reinterpret_cast< const pcl::PointCloud<pcl::PointXYZRGB>::Ptr(*)>(_a[1])),(*reinterpret_cast< const cv::Mat_<cv::Vec3b>(*)>(_a[2]))); break;
        case 1: _t->new_pose((*reinterpret_cast< const Eigen::Matrix4f(*)>(_a[1]))); break;
        case 2: _t->update_model_cloud((*reinterpret_cast< const boost::shared_ptr<Sensor::AlignedPointXYZRGBVector>(*)>(_a[1]))); break;
        case 3: _t->update_cam_trajectory((*reinterpret_cast< const boost::shared_ptr<std::vector<Sensor::CameraLocation> >(*)>(_a[1]))); break;
        case 4: _t->update_visualization(); break;
        case 5: _t->printStatus((*reinterpret_cast< const std::string(*)>(_a[1]))); break;
        case 6: _t->finishedOptimizeCameras((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->update_boundingbox((*reinterpret_cast< const std::vector<Eigen::Vector3f>(*)>(_a[1])),(*reinterpret_cast< const Eigen::Matrix4f(*)>(_a[2]))); break;
        case 8: _t->set_roi((*reinterpret_cast< const Eigen::Vector3f(*)>(_a[1])),(*reinterpret_cast< const Eigen::Vector3f(*)>(_a[2])),(*reinterpret_cast< const Eigen::Matrix4f(*)>(_a[3]))); break;
        case 9: _t->cam_params_changed((*reinterpret_cast< const RGBDCameraParameter(*)>(_a[1]))); break;
        case 10: _t->cam_tracker_params_changed((*reinterpret_cast< const CamaraTrackerParameter(*)>(_a[1]))); break;
        case 11: _t->bundle_adjustment_parameter_changed((*reinterpret_cast< const BundleAdjustmentParameter(*)>(_a[1]))); break;
        case 12: _t->select_roi((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 13: _t->set_roi_params((*reinterpret_cast< const double(*)>(_a[1])),(*reinterpret_cast< const double(*)>(_a[2])),(*reinterpret_cast< const double(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Sensor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Sensor::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_Sensor,
      qt_meta_data_Sensor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Sensor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Sensor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Sensor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Sensor))
        return static_cast<void*>(const_cast< Sensor*>(this));
    return QThread::qt_metacast(_clname);
}

int Sensor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void Sensor::new_image(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr & _t1, const cv::Mat_<cv::Vec3b> & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Sensor::new_pose(const Eigen::Matrix4f & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Sensor::update_model_cloud(const boost::shared_ptr<Sensor::AlignedPointXYZRGBVector> & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Sensor::update_cam_trajectory(const boost::shared_ptr<std::vector<Sensor::CameraLocation> > & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Sensor::update_visualization()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void Sensor::printStatus(const std::string & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Sensor::finishedOptimizeCameras(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Sensor::update_boundingbox(const std::vector<Eigen::Vector3f> & _t1, const Eigen::Matrix4f & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Sensor::set_roi(const Eigen::Vector3f & _t1, const Eigen::Vector3f & _t2, const Eigen::Matrix4f & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_END_MOC_NAMESPACE
