/****************************************************************************
** Meta object code from reading C++ file 'ObjectSegmentation.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../apps/RTMT/ObjectSegmentation.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ObjectSegmentation.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ObjectSegmentation[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      33,   20,   19,   19, 0x05,
     113,  103,   19,   19, 0x05,
     190,  185,   19,   19, 0x05,
     215,   19,   19,   19, 0x05,
     261,  238,   19,   19, 0x05,
     304,   19,   19,   19, 0x05,

 // slots: signature, parameters, type, tag, flags
     337,  333,   19,   19, 0x0a,
     368,  360,   19,   19, 0x0a,
     409,  405,   19,   19, 0x0a,
     436,  424,   19,   19, 0x0a,
     482,  476,   19,   19, 0x0a,
     536,  476,   19,   19, 0x0a,
     614,  588,   19,   19, 0x0a,
     719,  671,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ObjectSegmentation[] = {
    "ObjectSegmentation\0\0_cloud,image\0"
    "new_image(pcl::PointCloud<pcl::PointXYZRGB>::Ptr,cv::Mat_<cv::Vec3b>)\0"
    "_oc_cloud\0"
    "update_model_cloud(boost::shared_ptr<Sensor::AlignedPointXYZRGBVector>"
    ")\0"
    "_txt\0printStatus(std::string)\0"
    "update_visualization()\0_object_base_transform\0"
    "set_object_base_transform(Eigen::Matrix4f)\0"
    "finishedObjectSegmentation()\0x,y\0"
    "segment_image(int,int)\0x,y,val\0"
    "segment_image_rectangle(int,int,int)\0"
    "idx\0set_image(int)\0_cam_params\0"
    "cam_params_changed(RGBDCameraParameter)\0"
    "param\0segmentation_parameter_changed(SegmentationParameter)\0"
    "object_modelling_parameter_changed(ObjectModelling)\0"
    "_bb_min,_bb_max,_roi_pose\0"
    "set_roi(Eigen::Vector3f,Eigen::Vector3f,Eigen::Matrix4f)\0"
    "use_roi_segm,offs,_use_dense_mv,_edge_radius_px\0"
    "set_segmentation_params(bool,double,bool,double)\0"
};

void ObjectSegmentation::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ObjectSegmentation *_t = static_cast<ObjectSegmentation *>(_o);
        switch (_id) {
        case 0: _t->new_image((*reinterpret_cast< const pcl::PointCloud<pcl::PointXYZRGB>::Ptr(*)>(_a[1])),(*reinterpret_cast< const cv::Mat_<cv::Vec3b>(*)>(_a[2]))); break;
        case 1: _t->update_model_cloud((*reinterpret_cast< const boost::shared_ptr<Sensor::AlignedPointXYZRGBVector>(*)>(_a[1]))); break;
        case 2: _t->printStatus((*reinterpret_cast< const std::string(*)>(_a[1]))); break;
        case 3: _t->update_visualization(); break;
        case 4: _t->set_object_base_transform((*reinterpret_cast< const Eigen::Matrix4f(*)>(_a[1]))); break;
        case 5: _t->finishedObjectSegmentation(); break;
        case 6: _t->segment_image((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: _t->segment_image_rectangle((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 8: _t->set_image((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->cam_params_changed((*reinterpret_cast< const RGBDCameraParameter(*)>(_a[1]))); break;
        case 10: _t->segmentation_parameter_changed((*reinterpret_cast< const SegmentationParameter(*)>(_a[1]))); break;
        case 11: _t->object_modelling_parameter_changed((*reinterpret_cast< const ObjectModelling(*)>(_a[1]))); break;
        case 12: _t->set_roi((*reinterpret_cast< const Eigen::Vector3f(*)>(_a[1])),(*reinterpret_cast< const Eigen::Vector3f(*)>(_a[2])),(*reinterpret_cast< const Eigen::Matrix4f(*)>(_a[3]))); break;
        case 13: _t->set_segmentation_params((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< const double(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3])),(*reinterpret_cast< const double(*)>(_a[4]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ObjectSegmentation::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ObjectSegmentation::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_ObjectSegmentation,
      qt_meta_data_ObjectSegmentation, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ObjectSegmentation::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ObjectSegmentation::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ObjectSegmentation::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ObjectSegmentation))
        return static_cast<void*>(const_cast< ObjectSegmentation*>(this));
    return QThread::qt_metacast(_clname);
}

int ObjectSegmentation::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void ObjectSegmentation::new_image(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr & _t1, const cv::Mat_<cv::Vec3b> & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ObjectSegmentation::update_model_cloud(const boost::shared_ptr<Sensor::AlignedPointXYZRGBVector> & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ObjectSegmentation::printStatus(const std::string & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ObjectSegmentation::update_visualization()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void ObjectSegmentation::set_object_base_transform(const Eigen::Matrix4f & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void ObjectSegmentation::finishedObjectSegmentation()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}
QT_END_MOC_NAMESPACE
