/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../apps/RTMT/mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      35,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   12,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      31,   11,   11,   11, 0x08,
      64,   11,   11,   11, 0x08,
      90,   11,   11,   11, 0x08,
     112,   11,   11,   11, 0x08,
     133,   11,   11,   11, 0x08,
     159,   11,   11,   11, 0x08,
     184,   11,   11,   11, 0x08,
     211,   11,   11,   11, 0x08,
     238,   11,   11,   11, 0x08,
     267,   11,   11,   11, 0x08,
     297,   11,   11,   11, 0x08,
     320,   11,   11,   11, 0x08,
     346,   11,   11,   11, 0x08,
     369,   11,   11,   11, 0x08,
     394,   11,   11,   11, 0x08,
     422,   11,   11,   11, 0x08,
     451,   11,   11,   11, 0x08,
     477,   11,   11,   11, 0x08,
     505,   11,   11,   11, 0x08,
     528,   11,   11,   11, 0x08,
     564,  552,   11,   11, 0x08,
     593,   11,   11,   11, 0x08,
     622,   11,   11,   11, 0x08,
     654,  651,   11,   11, 0x08,
     678,   11,   11,   11, 0x08,
     705,   11,   11,   11, 0x08,
     725,   11,   11,   11, 0x08,
     750,   11,   11,   11, 0x08,
     772,   11,   11,   11, 0x08,
     800,   11,   11,   11, 0x08,
     824,   11,   11,   11, 0x08,
     850,   11,   11,   11, 0x08,
     876,   11,   11,   11, 0x08,
     910,  905,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0idx\0set_image(int)\0"
    "on_actionPreferences_triggered()\0"
    "on_actionExit_triggered()\0"
    "on_CamStart_clicked()\0on_CamStop_clicked()\0"
    "on_TrackerStart_clicked()\0"
    "on_TrackerStop_clicked()\0"
    "on_OptimizePoses_clicked()\0"
    "on_SegmentObject_clicked()\0"
    "on_SavePointClouds_clicked()\0"
    "on_SaveTrackerModel_clicked()\0"
    "on_ResetView_clicked()\0on_ResetTracker_clicked()\0"
    "on_ShowImage_clicked()\0on_ShowCameras_clicked()\0"
    "on_ShowPointCloud_clicked()\0"
    "on_ShowObjectModel_clicked()\0"
    "on_undoOptimize_clicked()\0"
    "on_okSegmentation_clicked()\0"
    "on_imForward_clicked()\0on_imBackward_clicked()\0"
    "num_cameras\0finishedOptimizeCameras(int)\0"
    "finishedStoreTrackingModel()\0"
    "finishedObjectSegmentation()\0ok\0"
    "finishedAlignment(bool)\0"
    "on_ShowDepthMask_clicked()\0"
    "on_setROI_clicked()\0on_ActivateROI_clicked()\0"
    "on_ResetROI_clicked()\0on_OptimizeObject_clicked()\0"
    "on_SessionAdd_clicked()\0"
    "on_SessionAlign_clicked()\0"
    "on_SessionClear_clicked()\0"
    "on_SessionOptimize_clicked()\0_txt\0"
    "printStatus(std::string)\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->set_image((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_actionPreferences_triggered(); break;
        case 2: _t->on_actionExit_triggered(); break;
        case 3: _t->on_CamStart_clicked(); break;
        case 4: _t->on_CamStop_clicked(); break;
        case 5: _t->on_TrackerStart_clicked(); break;
        case 6: _t->on_TrackerStop_clicked(); break;
        case 7: _t->on_OptimizePoses_clicked(); break;
        case 8: _t->on_SegmentObject_clicked(); break;
        case 9: _t->on_SavePointClouds_clicked(); break;
        case 10: _t->on_SaveTrackerModel_clicked(); break;
        case 11: _t->on_ResetView_clicked(); break;
        case 12: _t->on_ResetTracker_clicked(); break;
        case 13: _t->on_ShowImage_clicked(); break;
        case 14: _t->on_ShowCameras_clicked(); break;
        case 15: _t->on_ShowPointCloud_clicked(); break;
        case 16: _t->on_ShowObjectModel_clicked(); break;
        case 17: _t->on_undoOptimize_clicked(); break;
        case 18: _t->on_okSegmentation_clicked(); break;
        case 19: _t->on_imForward_clicked(); break;
        case 20: _t->on_imBackward_clicked(); break;
        case 21: _t->finishedOptimizeCameras((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->finishedStoreTrackingModel(); break;
        case 23: _t->finishedObjectSegmentation(); break;
        case 24: _t->finishedAlignment((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->on_ShowDepthMask_clicked(); break;
        case 26: _t->on_setROI_clicked(); break;
        case 27: _t->on_ActivateROI_clicked(); break;
        case 28: _t->on_ResetROI_clicked(); break;
        case 29: _t->on_OptimizeObject_clicked(); break;
        case 30: _t->on_SessionAdd_clicked(); break;
        case 31: _t->on_SessionAlign_clicked(); break;
        case 32: _t->on_SessionClear_clicked(); break;
        case 33: _t->on_SessionOptimize_clicked(); break;
        case 34: _t->printStatus((*reinterpret_cast< const std::string(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::set_image(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
