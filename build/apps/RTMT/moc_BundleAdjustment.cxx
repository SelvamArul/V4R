/****************************************************************************
** Meta object code from reading C++ file 'BundleAdjustment.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../apps/RTMT/BundleAdjustment.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'BundleAdjustment.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_BundleAdjustment[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      23,   18,   17,   17, 0x05,
      58,   48,   17,   17, 0x05,
     146,  130,   17,   17, 0x05,
     225,   17,   17,   17, 0x05,
     260,  248,   17,   17, 0x05,

 // slots: signature, parameters, type, tag, flags
     309,  289,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_BundleAdjustment[] = {
    "BundleAdjustment\0\0_txt\0printStatus(std::string)\0"
    "_oc_cloud\0"
    "update_model_cloud(boost::shared_ptr<Sensor::AlignedPointXYZRGBVector>"
    ")\0"
    "_cam_trajectory\0"
    "update_cam_trajectory(boost::shared_ptr<std::vector<Sensor::CameraLoca"
    "tion> >)\0"
    "update_visualization()\0num_cameras\0"
    "finishedOptimizeCameras(int)\0"
    "_cam_tracker_params\0"
    "cam_tracker_params_changed(CamaraTrackerParameter)\0"
};

void BundleAdjustment::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        BundleAdjustment *_t = static_cast<BundleAdjustment *>(_o);
        switch (_id) {
        case 0: _t->printStatus((*reinterpret_cast< const std::string(*)>(_a[1]))); break;
        case 1: _t->update_model_cloud((*reinterpret_cast< const boost::shared_ptr<Sensor::AlignedPointXYZRGBVector>(*)>(_a[1]))); break;
        case 2: _t->update_cam_trajectory((*reinterpret_cast< const boost::shared_ptr<std::vector<Sensor::CameraLocation> >(*)>(_a[1]))); break;
        case 3: _t->update_visualization(); break;
        case 4: _t->finishedOptimizeCameras((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->cam_tracker_params_changed((*reinterpret_cast< const CamaraTrackerParameter(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData BundleAdjustment::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject BundleAdjustment::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_BundleAdjustment,
      qt_meta_data_BundleAdjustment, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &BundleAdjustment::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *BundleAdjustment::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *BundleAdjustment::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_BundleAdjustment))
        return static_cast<void*>(const_cast< BundleAdjustment*>(this));
    return QThread::qt_metacast(_clname);
}

int BundleAdjustment::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void BundleAdjustment::printStatus(const std::string & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void BundleAdjustment::update_model_cloud(const boost::shared_ptr<Sensor::AlignedPointXYZRGBVector> & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void BundleAdjustment::update_cam_trajectory(const boost::shared_ptr<std::vector<Sensor::CameraLocation> > & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void BundleAdjustment::update_visualization()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void BundleAdjustment::finishedOptimizeCameras(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
