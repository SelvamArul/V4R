/****************************************************************************
** Meta object code from reading C++ file 'MultiSession.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../apps/RTMT/MultiSession.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MultiSession.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MultiSession[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   14,   13,   13, 0x05,
      47,   44,   13,   13, 0x05,
      81,   71,   13,   13, 0x05,
     153,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
     182,  176,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MultiSession[] = {
    "MultiSession\0\0_txt\0printStatus(std::string)\0"
    "ok\0finishedAlignment(bool)\0_oc_cloud\0"
    "update_model_cloud(boost::shared_ptr<Sensor::AlignedPointXYZRGBVector>"
    ")\0"
    "update_visualization()\0param\0"
    "object_modelling_parameter_changed(ObjectModelling)\0"
};

void MultiSession::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MultiSession *_t = static_cast<MultiSession *>(_o);
        switch (_id) {
        case 0: _t->printStatus((*reinterpret_cast< const std::string(*)>(_a[1]))); break;
        case 1: _t->finishedAlignment((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->update_model_cloud((*reinterpret_cast< const boost::shared_ptr<Sensor::AlignedPointXYZRGBVector>(*)>(_a[1]))); break;
        case 3: _t->update_visualization(); break;
        case 4: _t->object_modelling_parameter_changed((*reinterpret_cast< const ObjectModelling(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MultiSession::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MultiSession::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_MultiSession,
      qt_meta_data_MultiSession, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MultiSession::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MultiSession::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MultiSession::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MultiSession))
        return static_cast<void*>(const_cast< MultiSession*>(this));
    return QThread::qt_metacast(_clname);
}

int MultiSession::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void MultiSession::printStatus(const std::string & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MultiSession::finishedAlignment(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MultiSession::update_model_cloud(const boost::shared_ptr<Sensor::AlignedPointXYZRGBVector> & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MultiSession::update_visualization()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
QT_END_MOC_NAMESPACE
