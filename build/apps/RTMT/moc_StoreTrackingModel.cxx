/****************************************************************************
** Meta object code from reading C++ file 'StoreTrackingModel.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../apps/RTMT/StoreTrackingModel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'StoreTrackingModel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_StoreTrackingModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      25,   20,   19,   19, 0x05,
      50,   19,   19,   19, 0x05,

 // slots: signature, parameters, type, tag, flags
      91,   79,   19,   19, 0x0a,
     154,  131,   19,   19, 0x0a,
     215,  197,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_StoreTrackingModel[] = {
    "StoreTrackingModel\0\0_txt\0"
    "printStatus(std::string)\0"
    "finishedStoreTrackingModel()\0_cam_params\0"
    "cam_params_changed(RGBDCameraParameter)\0"
    "_object_base_transform\0"
    "set_object_base_transform(Eigen::Matrix4f)\0"
    "create_cb,rnn_thr\0set_cb_param(bool,float)\0"
};

void StoreTrackingModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        StoreTrackingModel *_t = static_cast<StoreTrackingModel *>(_o);
        switch (_id) {
        case 0: _t->printStatus((*reinterpret_cast< const std::string(*)>(_a[1]))); break;
        case 1: _t->finishedStoreTrackingModel(); break;
        case 2: _t->cam_params_changed((*reinterpret_cast< const RGBDCameraParameter(*)>(_a[1]))); break;
        case 3: _t->set_object_base_transform((*reinterpret_cast< const Eigen::Matrix4f(*)>(_a[1]))); break;
        case 4: _t->set_cb_param((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData StoreTrackingModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject StoreTrackingModel::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_StoreTrackingModel,
      qt_meta_data_StoreTrackingModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &StoreTrackingModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *StoreTrackingModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *StoreTrackingModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_StoreTrackingModel))
        return static_cast<void*>(const_cast< StoreTrackingModel*>(this));
    return QThread::qt_metacast(_clname);
}

int StoreTrackingModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void StoreTrackingModel::printStatus(const std::string & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void StoreTrackingModel::finishedStoreTrackingModel()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
