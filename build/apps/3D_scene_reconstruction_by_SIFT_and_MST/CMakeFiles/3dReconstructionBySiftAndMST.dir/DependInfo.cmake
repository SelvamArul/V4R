# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/local/munoza/software/v4r_final/apps/3D_scene_reconstruction_by_SIFT_and_MST/main.cpp" "/home/local/munoza/software/v4r_final/build/apps/3D_scene_reconstruction_by_SIFT_and_MST/CMakeFiles/3dReconstructionBySiftAndMST.dir/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "EIGEN_NO_DEBUG"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/local/munoza/software/v4r_final/build/modules/core/CMakeFiles/v4r_core.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/registration/CMakeFiles/v4r_registration.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/io/CMakeFiles/v4r_io.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../3rdparty/SiftGPU/src/siftgpu_external/src"
  "../modules/features/include"
  "../modules/common/include"
  "../modules/io/include"
  "../modules/registration/include"
  "../modules/core/include"
  "../apps/3D_scene_reconstruction_by_SIFT_and_MST"
  "."
  "/usr/local/include/eigen3"
  "/usr/include/vtk-5.8"
  "/usr/include/opencv"
  "/usr/include/pcl-1.7"
  "/usr/include/ni"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
