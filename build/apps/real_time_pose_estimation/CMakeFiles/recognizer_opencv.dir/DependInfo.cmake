# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/local/munoza/software/v4r_final/apps/real_time_pose_estimation/CsvReader.cpp" "/home/local/munoza/software/v4r_final/build/apps/real_time_pose_estimation/CMakeFiles/recognizer_opencv.dir/CsvReader.cpp.o"
  "/home/local/munoza/software/v4r_final/apps/real_time_pose_estimation/CsvWriter.cpp" "/home/local/munoza/software/v4r_final/build/apps/real_time_pose_estimation/CMakeFiles/recognizer_opencv.dir/CsvWriter.cpp.o"
  "/home/local/munoza/software/v4r_final/apps/real_time_pose_estimation/Mesh.cpp" "/home/local/munoza/software/v4r_final/build/apps/real_time_pose_estimation/CMakeFiles/recognizer_opencv.dir/Mesh.cpp.o"
  "/home/local/munoza/software/v4r_final/apps/real_time_pose_estimation/Model.cpp" "/home/local/munoza/software/v4r_final/build/apps/real_time_pose_estimation/CMakeFiles/recognizer_opencv.dir/Model.cpp.o"
  "/home/local/munoza/software/v4r_final/apps/real_time_pose_estimation/ModelRegistration.cpp" "/home/local/munoza/software/v4r_final/build/apps/real_time_pose_estimation/CMakeFiles/recognizer_opencv.dir/ModelRegistration.cpp.o"
  "/home/local/munoza/software/v4r_final/apps/real_time_pose_estimation/PnPProblem.cpp" "/home/local/munoza/software/v4r_final/build/apps/real_time_pose_estimation/CMakeFiles/recognizer_opencv.dir/PnPProblem.cpp.o"
  "/home/local/munoza/software/v4r_final/apps/real_time_pose_estimation/RobustMatcher.cpp" "/home/local/munoza/software/v4r_final/build/apps/real_time_pose_estimation/CMakeFiles/recognizer_opencv.dir/RobustMatcher.cpp.o"
  "/home/local/munoza/software/v4r_final/apps/real_time_pose_estimation/Utils.cpp" "/home/local/munoza/software/v4r_final/build/apps/real_time_pose_estimation/CMakeFiles/recognizer_opencv.dir/Utils.cpp.o"
  "/home/local/munoza/software/v4r_final/apps/real_time_pose_estimation/recognizer_opencv.cpp" "/home/local/munoza/software/v4r_final/build/apps/real_time_pose_estimation/CMakeFiles/recognizer_opencv.dir/recognizer_opencv.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "EIGEN_NO_DEBUG"
  "EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET"
  "__x86_64__"
  "linux"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/local/munoza/software/v4r_final/build/modules/core/CMakeFiles/v4r_core.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/registration/CMakeFiles/v4r_registration.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/io/CMakeFiles/v4r_io.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/common/CMakeFiles/v4r_common.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/features/CMakeFiles/v4r_features.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/keypoints/CMakeFiles/v4r_keypoints.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/reconstruction/CMakeFiles/v4r_reconstruction.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/recognition/CMakeFiles/v4r_recognition.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/3rdparty/EDT/CMakeFiles/edt.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/rendering/CMakeFiles/v4r_rendering.dir/DependInfo.cmake"
  "/home/local/munoza/software/v4r_final/build/modules/segmentation/CMakeFiles/v4r_segmentation.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../3rdparty"
  "../3rdparty/SiftGPU/src/siftgpu_external/src"
  "../modules/recognition/include"
  "../modules/reconstruction/include"
  "../modules/keypoints/include"
  "../modules/features/include"
  "../modules/common/include"
  "../modules/io/include"
  "../modules/registration/include"
  "../modules/core/include"
  "../apps/real_time_pose_estimation"
  "."
  "/usr/local/include/eigen3"
  "/usr/include/vtk-5.8"
  "/usr/include/opencv"
  "/usr/include/pcl-1.7"
  "/usr/include/ni"
  "/usr/include/glm"
  "/home/local/munoza/lib/usr/local/include"
  "apps/real_time_pose_estimation"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
