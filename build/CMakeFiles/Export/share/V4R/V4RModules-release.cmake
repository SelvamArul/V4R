#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "v4r_core" for configuration "Release"
set_property(TARGET v4r_core APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_core PROPERTIES
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE ""
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_core.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_core.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_core )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_core "${_IMPORT_PREFIX}/lib/libv4r_core.so.1.0.0" )

# Import target "v4r_io" for configuration "Release"
set_property(TARGET v4r_io APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_io PROPERTIES
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_program_options.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_regex.so;/usr/lib/x86_64-linux-gnu/libpthread.so"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_io.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_io.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_io )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_io "${_IMPORT_PREFIX}/lib/libv4r_io.so.1.0.0" )

# Import target "v4r_common" for configuration "Release"
set_property(TARGET v4r_common APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_common PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_nonfree;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d;vtkCommon;vtkRendering;vtkHybrid;vtkCharts"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/lib/libpcl_common.so;/usr/lib/libOpenNI.so;/usr/lib/libpcl_io.so;/usr/lib/x86_64-linux-gnu/libflann_cpp_s.a;/usr/lib/libpcl_kdtree.so;/usr/lib/libpcl_search.so;/usr/lib/libpcl_sample_consensus.so;/usr/lib/libpcl_filters.so;/usr/lib/libpcl_features.so;/usr/lib/libpcl_keypoints.so;/usr/lib/libpcl_segmentation.so;/usr/lib/libpcl_visualization.so;/usr/lib/libpcl_outofcore.so;/usr/lib/libpcl_registration.so;/usr/lib/libpcl_recognition.so;/usr/lib/x86_64-linux-gnu/libqhull.so;/usr/lib/libpcl_surface.so;/usr/lib/libpcl_people.so;/usr/lib/libpcl_tracking.so;/usr/lib/libpcl_apps.so"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_common.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_common.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_common )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_common "${_IMPORT_PREFIX}/lib/libv4r_common.so.1.0.0" )

# Import target "v4r_features" for configuration "Release"
set_property(TARGET v4r_features APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_features PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_nonfree;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d;vtkCommon;vtkRendering;vtkHybrid;vtkCharts"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;v4r_common;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/lib/libpcl_common.so;/usr/lib/libOpenNI.so;/usr/lib/libpcl_io.so;/usr/lib/x86_64-linux-gnu/libflann_cpp_s.a;/usr/lib/libpcl_kdtree.so;/usr/lib/libpcl_search.so;/usr/lib/libpcl_sample_consensus.so;/usr/lib/libpcl_filters.so;/usr/lib/libpcl_features.so;/usr/lib/libpcl_keypoints.so;/usr/lib/libpcl_segmentation.so;/usr/lib/libpcl_visualization.so;/usr/lib/libpcl_outofcore.so;/usr/lib/libpcl_registration.so;/usr/lib/libpcl_recognition.so;/usr/lib/x86_64-linux-gnu/libqhull.so;/usr/lib/libpcl_surface.so;/usr/lib/libpcl_people.so;/usr/lib/libpcl_tracking.so;/usr/lib/libpcl_apps.so;GLEW;IL"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_features.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_features.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_features )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_features "${_IMPORT_PREFIX}/lib/libv4r_features.so.1.0.0" )

# Import target "v4r_keypoints" for configuration "Release"
set_property(TARGET v4r_keypoints APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_keypoints PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_nonfree;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d;vtkCommon;vtkRendering;vtkHybrid;vtkCharts"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;v4r_common;v4r_features;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/lib/libpcl_common.so;/usr/lib/libOpenNI.so;/usr/lib/libpcl_io.so;/usr/lib/x86_64-linux-gnu/libflann_cpp_s.a;/usr/lib/libpcl_kdtree.so;/usr/lib/libpcl_search.so;/usr/lib/libpcl_sample_consensus.so;/usr/lib/libpcl_filters.so;/usr/lib/libpcl_features.so;/usr/lib/libpcl_keypoints.so;/usr/lib/libpcl_segmentation.so;/usr/lib/libpcl_visualization.so;/usr/lib/libpcl_outofcore.so;/usr/lib/libpcl_registration.so;/usr/lib/libpcl_recognition.so;/usr/lib/x86_64-linux-gnu/libqhull.so;/usr/lib/libpcl_surface.so;/usr/lib/libpcl_people.so;/usr/lib/libpcl_tracking.so;/usr/lib/libpcl_apps.so;GLEW;IL"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_keypoints.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_keypoints.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_keypoints )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_keypoints "${_IMPORT_PREFIX}/lib/libv4r_keypoints.so.1.0.0" )

# Import target "v4r_ml" for configuration "Release"
set_property(TARGET v4r_ml APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_ml PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "libsvm;opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_nonfree;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d;vtkCommon;vtkRendering;vtkHybrid;vtkCharts"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;v4r_io;v4r_common;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_program_options.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_regex.so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;/usr/lib/libpcl_common.so;/usr/lib/libOpenNI.so;/usr/lib/libpcl_io.so;/usr/lib/x86_64-linux-gnu/libflann_cpp_s.a;/usr/lib/libpcl_kdtree.so;/usr/lib/libpcl_search.so;/usr/lib/libpcl_sample_consensus.so;/usr/lib/libpcl_filters.so;/usr/lib/libpcl_features.so;/usr/lib/libpcl_keypoints.so;/usr/lib/libpcl_segmentation.so;/usr/lib/libpcl_visualization.so;/usr/lib/libpcl_outofcore.so;/usr/lib/libpcl_registration.so;/usr/lib/libpcl_recognition.so;/usr/lib/x86_64-linux-gnu/libqhull.so;/usr/lib/libpcl_surface.so;/usr/lib/libpcl_people.so;/usr/lib/libpcl_tracking.so;/usr/lib/libpcl_apps.so"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_ml.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_ml.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_ml )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_ml "${_IMPORT_PREFIX}/lib/libv4r_ml.so.1.0.0" )

# Import target "v4r_reconstruction" for configuration "Release"
set_property(TARGET v4r_reconstruction APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_reconstruction PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_nonfree;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d;vtkCommon;vtkRendering;vtkHybrid;vtkCharts"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;v4r_io;v4r_common;v4r_features;v4r_keypoints;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_program_options.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_regex.so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;/usr/lib/libpcl_common.so;/usr/lib/libOpenNI.so;/usr/lib/libpcl_io.so;/usr/lib/x86_64-linux-gnu/libflann_cpp_s.a;/usr/lib/libpcl_kdtree.so;/usr/lib/libpcl_search.so;/usr/lib/libpcl_sample_consensus.so;/usr/lib/libpcl_filters.so;/usr/lib/libpcl_features.so;/usr/lib/libpcl_keypoints.so;/usr/lib/libpcl_segmentation.so;/usr/lib/libpcl_visualization.so;/usr/lib/libpcl_outofcore.so;/usr/lib/libpcl_registration.so;/usr/lib/libpcl_recognition.so;/usr/lib/x86_64-linux-gnu/libqhull.so;/usr/lib/libpcl_surface.so;/usr/lib/libpcl_people.so;/usr/lib/libpcl_tracking.so;/usr/lib/libpcl_apps.so;GLEW;IL"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_reconstruction.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_reconstruction.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_reconstruction )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_reconstruction "${_IMPORT_PREFIX}/lib/libv4r_reconstruction.so.1.0.0" )

# Import target "v4r_registration" for configuration "Release"
set_property(TARGET v4r_registration APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_registration PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_nonfree;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d;vtkCommon;vtkRendering;vtkHybrid;vtkCharts"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;v4r_io;v4r_common;v4r_features;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_program_options.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_regex.so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;/usr/lib/libpcl_common.so;/usr/lib/libOpenNI.so;/usr/lib/libpcl_io.so;/usr/lib/x86_64-linux-gnu/libflann_cpp_s.a;/usr/lib/libpcl_kdtree.so;/usr/lib/libpcl_search.so;/usr/lib/libpcl_sample_consensus.so;/usr/lib/libpcl_filters.so;/usr/lib/libpcl_features.so;/usr/lib/libpcl_keypoints.so;/usr/lib/libpcl_segmentation.so;/usr/lib/libpcl_visualization.so;/usr/lib/libpcl_outofcore.so;/usr/lib/libpcl_registration.so;/usr/lib/libpcl_recognition.so;/usr/lib/x86_64-linux-gnu/libqhull.so;/usr/lib/libpcl_surface.so;/usr/lib/libpcl_people.so;/usr/lib/libpcl_tracking.so;/usr/lib/libpcl_apps.so;GLEW;IL"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_registration.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_registration.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_registration )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_registration "${_IMPORT_PREFIX}/lib/libv4r_registration.so.1.0.0" )

# Import target "v4r_rendering" for configuration "Release"
set_property(TARGET v4r_rendering APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_rendering PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_nonfree;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d;vtkCommon;vtkRendering;vtkHybrid;vtkCharts"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;v4r_common;/usr/lib/libassimp.so;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_program_options.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_regex.so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/lib/x86_64-linux-gnu/libGLEW.so;/usr/lib/x86_64-linux-gnu/libGLU.so;/usr/lib/x86_64-linux-gnu/libGL.so;/usr/lib/x86_64-linux-gnu/libSM.so;/usr/lib/x86_64-linux-gnu/libICE.so;/usr/lib/x86_64-linux-gnu/libX11.so;/usr/lib/x86_64-linux-gnu/libXext.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;/usr/lib/libpcl_common.so;/usr/lib/libOpenNI.so;/usr/lib/libpcl_io.so;/usr/lib/x86_64-linux-gnu/libflann_cpp_s.a;/usr/lib/libpcl_kdtree.so;/usr/lib/libpcl_search.so;/usr/lib/libpcl_sample_consensus.so;/usr/lib/libpcl_filters.so;/usr/lib/libpcl_features.so;/usr/lib/libpcl_keypoints.so;/usr/lib/libpcl_segmentation.so;/usr/lib/libpcl_visualization.so;/usr/lib/libpcl_outofcore.so;/usr/lib/libpcl_registration.so;/usr/lib/libpcl_recognition.so;/usr/lib/x86_64-linux-gnu/libqhull.so;/usr/lib/libpcl_surface.so;/usr/lib/libpcl_people.so;/usr/lib/libpcl_tracking.so;/usr/lib/libpcl_apps.so"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_rendering.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_rendering.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_rendering )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_rendering "${_IMPORT_PREFIX}/lib/libv4r_rendering.so.1.0.0" )

# Import target "v4r_segmentation" for configuration "Release"
set_property(TARGET v4r_segmentation APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_segmentation PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_nonfree;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d;vtkCommon;vtkRendering;vtkHybrid;vtkCharts"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;v4r_io;v4r_common;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_program_options.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_regex.so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;/usr/lib/libpcl_common.so;/usr/lib/libOpenNI.so;/usr/lib/libpcl_io.so;/usr/lib/x86_64-linux-gnu/libflann_cpp_s.a;/usr/lib/libpcl_kdtree.so;/usr/lib/libpcl_search.so;/usr/lib/libpcl_sample_consensus.so;/usr/lib/libpcl_filters.so;/usr/lib/libpcl_features.so;/usr/lib/libpcl_keypoints.so;/usr/lib/libpcl_segmentation.so;/usr/lib/libpcl_visualization.so;/usr/lib/libpcl_outofcore.so;/usr/lib/libpcl_registration.so;/usr/lib/libpcl_recognition.so;/usr/lib/x86_64-linux-gnu/libqhull.so;/usr/lib/libpcl_surface.so;/usr/lib/libpcl_people.so;/usr/lib/libpcl_tracking.so;/usr/lib/libpcl_apps.so"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_segmentation.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_segmentation.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_segmentation )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_segmentation "${_IMPORT_PREFIX}/lib/libv4r_segmentation.so.1.0.0" )

# Import target "v4r_tracking" for configuration "Release"
set_property(TARGET v4r_tracking APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_tracking PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_nonfree;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d;vtkCommon;vtkRendering;vtkHybrid;vtkCharts"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;v4r_io;v4r_common;v4r_features;v4r_keypoints;v4r_reconstruction;v4r_registration;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_program_options.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_regex.so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;/usr/lib/libpcl_common.so;/usr/lib/libOpenNI.so;/usr/lib/libpcl_io.so;/usr/lib/x86_64-linux-gnu/libflann_cpp_s.a;/usr/lib/libpcl_kdtree.so;/usr/lib/libpcl_search.so;/usr/lib/libpcl_sample_consensus.so;/usr/lib/libpcl_filters.so;/usr/lib/libpcl_features.so;/usr/lib/libpcl_keypoints.so;/usr/lib/libpcl_segmentation.so;/usr/lib/libpcl_visualization.so;/usr/lib/libpcl_outofcore.so;/usr/lib/libpcl_registration.so;/usr/lib/libpcl_recognition.so;/usr/lib/x86_64-linux-gnu/libqhull.so;/usr/lib/libpcl_surface.so;/usr/lib/libpcl_people.so;/usr/lib/libpcl_tracking.so;/usr/lib/libpcl_apps.so;GLEW;IL"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_tracking.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_tracking.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_tracking )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_tracking "${_IMPORT_PREFIX}/lib/libv4r_tracking.so.1.0.0" )

# Import target "v4r_object_modelling" for configuration "Release"
set_property(TARGET v4r_object_modelling APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_object_modelling PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_nonfree;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d;vtkCommon;vtkRendering;vtkHybrid;vtkCharts"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;v4r_io;v4r_common;v4r_features;v4r_keypoints;v4r_registration;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_program_options.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_regex.so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;/usr/lib/libpcl_common.so;/usr/lib/libOpenNI.so;/usr/lib/libpcl_io.so;/usr/lib/x86_64-linux-gnu/libflann_cpp_s.a;/usr/lib/libpcl_kdtree.so;/usr/lib/libpcl_search.so;/usr/lib/libpcl_sample_consensus.so;/usr/lib/libpcl_filters.so;/usr/lib/libpcl_features.so;/usr/lib/libpcl_keypoints.so;/usr/lib/libpcl_segmentation.so;/usr/lib/libpcl_visualization.so;/usr/lib/libpcl_outofcore.so;/usr/lib/libpcl_registration.so;/usr/lib/libpcl_recognition.so;/usr/lib/x86_64-linux-gnu/libqhull.so;/usr/lib/libpcl_surface.so;/usr/lib/libpcl_people.so;/usr/lib/libpcl_tracking.so;/usr/lib/libpcl_apps.so;GLEW;IL"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_object_modelling.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_object_modelling.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_object_modelling )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_object_modelling "${_IMPORT_PREFIX}/lib/libv4r_object_modelling.so.1.0.0" )

# Import target "v4r_recognition" for configuration "Release"
set_property(TARGET v4r_recognition APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(v4r_recognition PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "opencv_videostab;opencv_video;opencv_ts;opencv_superres;opencv_stitching;opencv_photo;opencv_ocl;opencv_objdetect;opencv_nonfree;opencv_ml;opencv_legacy;opencv_imgproc;opencv_highgui;opencv_gpu;opencv_flann;opencv_features2d;opencv_core;opencv_contrib;opencv_calib3d;vtkCommon;vtkRendering;vtkHybrid;vtkCharts"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "v4r_core;v4r_io;v4r_common;v4r_features;v4r_keypoints;v4r_registration;v4r_rendering;v4r_segmentation;/usr/lib/libassimp.so;/usr/lib/x86_64-linux-gnu/libboost_thread.so;/usr/lib/x86_64-linux-gnu/libboost_program_options.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libboost_system.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_regex.so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/lib/x86_64-linux-gnu/libGLEW.so;/usr/lib/x86_64-linux-gnu/libglog.so;/usr/lib/x86_64-linux-gnu/libGLU.so;/usr/lib/x86_64-linux-gnu/libGL.so;/usr/lib/x86_64-linux-gnu/libSM.so;/usr/lib/x86_64-linux-gnu/libICE.so;/usr/lib/x86_64-linux-gnu/libX11.so;/usr/lib/x86_64-linux-gnu/libXext.so;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;/usr/lib/libpcl_common.so;/usr/lib/libOpenNI.so;/usr/lib/libpcl_io.so;/usr/lib/x86_64-linux-gnu/libflann_cpp_s.a;/usr/lib/libpcl_kdtree.so;/usr/lib/libpcl_search.so;/usr/lib/libpcl_sample_consensus.so;/usr/lib/libpcl_filters.so;/usr/lib/libpcl_features.so;/usr/lib/libpcl_keypoints.so;/usr/lib/libpcl_segmentation.so;/usr/lib/libpcl_visualization.so;/usr/lib/libpcl_outofcore.so;/usr/lib/libpcl_registration.so;/usr/lib/libpcl_recognition.so;/usr/lib/x86_64-linux-gnu/libqhull.so;/usr/lib/libpcl_surface.so;/usr/lib/libpcl_people.so;/usr/lib/libpcl_tracking.so;/usr/lib/libpcl_apps.so;GLEW;IL"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libv4r_recognition.so.1.0.0"
  IMPORTED_SONAME_RELEASE "libv4r_recognition.so.1.0"
  )

list(APPEND _IMPORT_CHECK_TARGETS v4r_recognition )
list(APPEND _IMPORT_CHECK_FILES_FOR_v4r_recognition "${_IMPORT_PREFIX}/lib/libv4r_recognition.so.1.0.0" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
